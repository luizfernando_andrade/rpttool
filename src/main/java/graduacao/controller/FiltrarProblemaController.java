package graduacao.controller;

import graduacao.dao.MauCheiroDao;
import graduacao.modelo.MauCheiro;
import graduacao.modelo.TipoProblemaRefatoracao;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(value="/filtrarProblemaController")
public class FiltrarProblemaController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException, NullPointerException {
		
		Integer tipoProblema = Integer.parseInt(request.getParameter("tipoProblema"));
		
		List<MauCheiro> mausCheiros = MauCheiroDao.getInstance().recuperarPorTipoProblema(tipoProblema);
		
		request.setAttribute("mausCheiros", mausCheiros);
		
		TipoProblemaRefatoracao tipoProblemaRefatoracao = 
				mausCheiros.get(0).getRefatoracaoPadrao().getTipoProblema();
		
		request.setAttribute("tipoProblemaRefatoracao", tipoProblemaRefatoracao);
		request.setAttribute("mauCheiro", null);
		
		RequestDispatcher rd = request.getRequestDispatcher("problemas.jsp");
		rd.forward(request, response);
		
	}

}
