package graduacao.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import graduacao.dao.MauCheiroDao;
import graduacao.modelo.MauCheiro;
import graduacao.modelo.RefatoracaoPadrao;
import graduacao.modelo.TipoProblemaRefatoracao;

@WebServlet(value="/direcionarRefatoracao")
public class DirecionarRefatoracaoController extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) 
					throws ServletException, IOException {
	
		Integer idMauCheiro = Integer.parseInt(request.getParameter("refatoracao"));
		
		MauCheiro mauCheiro = MauCheiroDao.getInstance().recuperarPorId(idMauCheiro);
		
		
		TipoProblemaRefatoracao tipoProblemaRefatoracao = 
				mauCheiro.getRefatoracaoPadrao().getTipoProblema();
		
		RefatoracaoPadrao refatoracaoPadrao = mauCheiro.getRefatoracaoPadrao();
		
		request.setAttribute("refatoracaoPadrao", refatoracaoPadrao);
		request.setAttribute("mauCheiro", mauCheiro);
		request.setAttribute("tipoProblemaRefatoracao", tipoProblemaRefatoracao);
		
		RequestDispatcher rd = request.getRequestDispatcher("refatoracao.jsp");
		rd.forward(request, response);
	}
	

}
