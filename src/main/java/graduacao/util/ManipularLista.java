package graduacao.util;

import java.util.List;

import graduacao.modelo.PassoRefatoracao;

public class ManipularLista {

	private static ManipularLista instance = new ManipularLista();

	private List<PassoRefatoracao> passos;
	
	private int index = 0;
	
	public static ManipularLista getInstance() {
		if (instance == null){
			instance = new ManipularLista();
		}
		return instance;		
	}
	
	private ManipularLista(){}

	public List<PassoRefatoracao> getPassos() {
		return passos;
	}

	public void setPassos(List<PassoRefatoracao> passos) {
		this.passos = passos;
	}

	public int addIndex() {
		if (index < passos.size() - 1){
			index = index + 1;
			return index;
		}
		return index;
	}
	
	public int subIndex() {
		if (index > 0){
			index = index - 1;
			return index;
		}
		return index;
	}

}
