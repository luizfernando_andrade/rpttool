package graduacao.modelo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class MauCheiro implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6702505711472183806L;

	@Id
	@GeneratedValue
	private Integer id;
	
	@Column
	private String descricao;
	
	@OneToOne
	private RefatoracaoPadrao refatoracaoPadrao;
	
	@Column
	private String nomeImagem; 

	public String getNomeImagem() {
		return nomeImagem;
	}

	public void setNomeImagem(String nomeImagem) {
		this.nomeImagem = nomeImagem;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public RefatoracaoPadrao getRefatoracaoPadrao() {
		return refatoracaoPadrao;
	}

	public void setRefatoracaoPadrao(RefatoracaoPadrao refatoracaoPadrao) {
		this.refatoracaoPadrao = refatoracaoPadrao;
	}
	
	
}
