package graduacao.modelo;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class TipoProblemaRefatoracao {

	@Id
	@GeneratedValue
	private Integer id;
	
	@Column
	private String descricao;
	
	@Column
	private String nomeTela;
	
	@OneToMany(mappedBy="tipoProblema")
	private List<RefatoracaoPadrao> refatoracoesPadrao;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getNomeTela() {
		return nomeTela;
	}
	public void setNomeTela(String nomeTela) {
		this.nomeTela = nomeTela;
	}
	public List<RefatoracaoPadrao> getRefatoracoesPadrao() {
		return refatoracoesPadrao;
	}
	public void setRefatoracoesPadrao(List<RefatoracaoPadrao> refatoracoesPadrao) {
		this.refatoracoesPadrao = refatoracoesPadrao;
	}
	
}
