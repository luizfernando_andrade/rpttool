package graduacao.modelo;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class RefatoracaoPadrao {
	
	@Id
	@GeneratedValue
	private Integer id;
	
	@OneToMany
	private List<PassoRefatoracao> passosRefatoracao;
	
	@ManyToOne
	private TipoProblemaRefatoracao tipoProblema;
	
	@OneToOne
	private MauCheiro mauCheiro;
	
	@Column
	private String descricao;
	
	@Column
	private String nomeTela;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<PassoRefatoracao> getPassosRefatoracao() {
		return passosRefatoracao;
	}

	public void setPassosRefatoracao(List<PassoRefatoracao> passosRefatoracao) {
		this.passosRefatoracao = passosRefatoracao;
	}

	public TipoProblemaRefatoracao getTipoProblema() {
		return tipoProblema;
	}

	public void setTipoProblema(TipoProblemaRefatoracao tipoProblema) {
		this.tipoProblema = tipoProblema;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getNomeTela() {
		return nomeTela;
	}

	public void setNomeTela(String nomeTela) {
		this.nomeTela = nomeTela;
	}
}
