package graduacao.factory;




public class PageFactory {

	private static PageFactory instance = new PageFactory();

	public static PageFactory getInstance() {
		if (instance == null){
			instance = new PageFactory();
		}
		
		return instance;		
	}
	
	private PageFactory(){}
	
	public String createPage(String page){
		if (page!=null){
			return page.concat(".jsp");
		}
		return null;
	}
}
