package graduacao.factory;




public class ViewFactory {

	private static ViewFactory instance = new ViewFactory();

	public static ViewFactory getInstance() {
		if (instance == null){
			instance = new ViewFactory();
		}
		
		return instance;		
	}
	
	private ViewFactory(){}
	
	public String createPage(String page){
		if (page!=null){
			return page.concat(".jsp");
		}
		return null;
	}
}
