package graduacao.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import graduacao.modelo.PassoRefatoracao;

public class PassoRefatoracaoDao extends AbstractDao<PassoRefatoracao> {

	private static PassoRefatoracaoDao instance = new PassoRefatoracaoDao();

	public static PassoRefatoracaoDao getInstance() {
		if (instance == null){
			instance = new PassoRefatoracaoDao();
		}
		return instance;		
	}
	
	private PassoRefatoracaoDao(){}

	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PassoRefatoracao> recuperarTodos() {
		try {
			em = getEmf().createEntityManager();
			Query hql = em.createQuery("from PassoRefatoracao");
			List<PassoRefatoracao> resultList = (List<PassoRefatoracao>) hql.getResultList();
			return resultList;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			em.close();
		}
		
	}

	@Override
	public PassoRefatoracao recuperarPorId(int id) {
		try{
			em = getEmf().createEntityManager();
			Query hql = em.createQuery("FROM PassoRefatoracao WHERE id = :id");
			hql.setParameter("id", id);
			return (PassoRefatoracao) hql.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<PassoRefatoracao> recuperarPorRefatoracaoPadrao(int idRefatoracaoPadrao) {
		try{
			em = getEmf().createEntityManager();
			Query hql = em.createQuery("FROM PassoRefatoracao "
					+ "WHERE refatoracaoPadrao.id = :idRefatoracaoPadrao");
			hql.setParameter("idRefatoracaoPadrao", idRefatoracaoPadrao);
			return (List<PassoRefatoracao>) hql.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			em.close();
		}
	}

	@Override
	public void gravar(PassoRefatoracao t) {
		// TODO Auto-generated method stub
		
	}

}
