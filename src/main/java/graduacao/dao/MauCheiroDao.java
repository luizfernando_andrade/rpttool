package graduacao.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import graduacao.modelo.MauCheiro;

public class MauCheiroDao extends AbstractDao<MauCheiro> {

	private static MauCheiroDao instance = new MauCheiroDao();

	public static MauCheiroDao getInstance() {
		if (instance == null){
			instance = new MauCheiroDao();
		}
		return instance;		
	}
	
	private MauCheiroDao(){}

	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<MauCheiro> recuperarTodos() {
		try {
			em = getEmf().createEntityManager();
			Query hql = em.createQuery("from MauCheiro");
			List<MauCheiro> resultList = (List<MauCheiro>) hql.getResultList();
			return resultList;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			em.close();
		}
	}

	@Override
	public MauCheiro recuperarPorId(int id) {
		try{
			em = getEmf().createEntityManager();
			Query hql = em.createQuery("FROM MauCheiro WHERE id = :id");
			hql.setParameter("id", id);
			return (MauCheiro) hql.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<MauCheiro> recuperarPorTipoProblema(int tipoProblema) {
		try{
			em = getEmf().createEntityManager();
			Query hql = em.createQuery("SELECT M FROM MauCheiro M "
					+ "JOIN M.refatoracaoPadrao R WHERE R.tipoProblema.id = :tipoProblema");
			hql.setParameter("tipoProblema", tipoProblema);
			
			List<MauCheiro> resultList = cast(hql.getResultList());
			return resultList;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	
	private List<MauCheiro> cast(List<Object> list) {
		List<MauCheiro> result = new ArrayList<MauCheiro>(list.size());
		for(Object o: list) {
			result.add((MauCheiro) o);
		}
		return result;
	}
	
	@Override
	public void gravar(MauCheiro t) {
		// TODO Auto-generated method stub
		
	}

}
