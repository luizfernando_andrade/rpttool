package graduacao.dao;

import java.util.List;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public abstract class AbstractDao<T> {

	public abstract List<T> recuperarTodos();
	
	public abstract T recuperarPorId(int id);
	
	public abstract void gravar(T t);
	
	private static final EntityManagerFactory emf = Persistence
			.createEntityManagerFactory("rpttool");

	public static EntityManagerFactory getEmf() {
		return emf;
	}
}
