package graduacao.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import graduacao.modelo.TipoProblemaRefatoracao;

public class TipoProblemaRefatoracaoDao extends AbstractDao<TipoProblemaRefatoracao> {
	
	private static TipoProblemaRefatoracaoDao instance = new TipoProblemaRefatoracaoDao();

	public static TipoProblemaRefatoracaoDao getInstance() {
		if (instance == null){
			instance = new TipoProblemaRefatoracaoDao();
		}
		return instance;		
	}
	
	private TipoProblemaRefatoracaoDao(){}
	
	EntityManager em;
	
	@Override
	public List<TipoProblemaRefatoracao> recuperarTodos() {
		try {
			em = getEmf().createEntityManager();
			return em.createQuery("from", TipoProblemaRefatoracao.class).getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			em.close();
		}
	}

	@Override
	public TipoProblemaRefatoracao recuperarPorId(int id) {
		try{
			em = getEmf().createEntityManager();
			Query hql = em.createQuery("FROM TipoProblemaRefatoracao WHERE id = :id");
			hql.setParameter("id", id);
			return (TipoProblemaRefatoracao) hql.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void gravar(TipoProblemaRefatoracao t) {
		// TODO Auto-generated method stub
		
	}

}
