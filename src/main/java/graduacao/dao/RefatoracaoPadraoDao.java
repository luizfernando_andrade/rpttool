package graduacao.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import graduacao.modelo.RefatoracaoPadrao;

public class RefatoracaoPadraoDao extends AbstractDao<RefatoracaoPadrao>{

	private static RefatoracaoPadraoDao instance = new RefatoracaoPadraoDao();

	public static RefatoracaoPadraoDao getInstance() {
		if (instance == null){
			instance = new RefatoracaoPadraoDao();
		}
		return instance;		
	}
	
	private RefatoracaoPadraoDao(){}

	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<RefatoracaoPadrao> recuperarTodos() {
		try {
			em = getEmf().createEntityManager();
			Query hql = em.createQuery("from RefatoracaoPadrao");
			List<RefatoracaoPadrao> resultList = (List<RefatoracaoPadrao>) hql.getResultList();
			return resultList;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			em.close();
		}
	}
	

	public RefatoracaoPadrao recuperarPorNome(String nome) {
		try{
			em = getEmf().createEntityManager();
			Query hql = em.createQuery("FROM RefatoracaoPadrao WHERE nomeTela = :nome");
			hql.setParameter("nome", nome);
			return (RefatoracaoPadrao) hql.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	

	@Override
	public RefatoracaoPadrao recuperarPorId(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void gravar(RefatoracaoPadrao t) {
		// TODO Auto-generated method stub
		
	}

}
