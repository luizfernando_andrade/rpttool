<%@ tag language="java" pageEncoding="ISO-8859-1"%>


<div style="width:720px;height:540px;">

<?prettify?>
<pre class="prettyprint">
public class AcessPoint {

	IPV6 ipv6 = new IPV6();
	IPV4 ipv4 = new IPV4();
	boolean possuiIPV4 = true;
	public void criarIPV4(String ip, String mask){
		ipv4.setEndereco(ip);
		ipv4.setMascara(mask);
	}
	public void criarIPV6(String ip, String mask){
		ipv6.setEndereco(ip);
		ipv6.setMascara(mask);
		possuiIPV4 = false;
	}
	public String direcionarIP(){
		String ip = null;
		if (possuiIPV4){
			ip = ipv4.direcionarIP(ipv4);
		} else {
			ip = ipv6.direcionarIPV6(ipv6);
		}
		return ip;
	}
}
</pre>

<script src="prettify/prettify_run.js"></script>