<%@ tag language="java" pageEncoding="ISO-8859-1"%>


<div style="width:720px;height:540px;">

<?prettify?>
<pre class="prettyprint">
public class TesteConexaoBD {

	@Test
	public void TestarConexaoCertificacao(){
		ConfiguraConexao configuraConexao = new ConfiguraConexao();
		configuraConexao.setUsuario("root");
		configuraConexao.setSenha("12345678909");
		configuraConexao.setBaseDados(TipoBaseDados.CERTIFICACAO);
		
		TipoConexao tipoConexao = new TipoConexao(configuraConexao);
		
		Conector conector = new Conector(tipoConexao);
		
		Conexao conexao = conector.criarConexao();
		
		Assert.assertTrue(conexao.getUrlConexao().contains("cert"));
	}
}
	
</pre>

<script src="prettify/prettify_run.js"></script>