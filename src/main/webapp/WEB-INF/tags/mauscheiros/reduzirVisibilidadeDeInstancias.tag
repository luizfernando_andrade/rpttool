<%@ tag language="java" pageEncoding="ISO-8859-1"%>


<div style="width:720px;height:540px;">

<?prettify?>
<pre class="prettyprint">
public class TestarMontagemEquipamento {

	@Test
	public void testarMontagem(){
		MontarEquipamento equipamento = new MontarEquipamento();
		List&lt;Componente&gt; componentes = new ArrayList&lt;&gt;();
		
		componentes.add(new ComponenteEletronico("fuz�vel"));
		componentes.add(new ComponenteMecanico("Alavanca disjuntor"));
		componentes.add(new ComponentePeriferico("Cabo de alta tens�o"));
		
		equipamento.setDescricao("Painel el�trico");
		equipamento.setComponentes(componentes);
		
		Assert.assertTrue(equipamento.getComponentes() != null
				&& equipamento.getComponentes().size() == 3);
	}
}	
</pre>

<script src="prettify/prettify_run.js"></script>