<%@ tag language="java" pageEncoding="ISO-8859-1"%>


<div style="width:720px;height:540px;">

<?prettify?>
<pre class="prettyprint">

public class Expressao {

	HashMap&lt;String, Double&gt; eixoX = new HashMap&lt;&gt;();
	
	public Map&lt;String, Double&gt; calcularBhaskara(Double a, Double b, Double c){
		
		DeltaComposite delta = new DeltaComposite(a, b, c);
		BhaskaraX1 x1 = new BhaskaraX1(a, b, delta);
		BhaskaraX2 x2 = new BhaskaraX2(a, b, delta);
		eixoX.put("x1", x1.calcularBhaskaraX1());
		eixoX.put("x2", x2.calcularBhaskaraX2());
		
		return eixoX;
	}
	
}

</pre>

<script src="prettify/prettify_run.js"></script>