<%@ tag language="java" pageEncoding="ISO-8859-1"%>


<div style="width:720px;height:540px;">

<?prettify?>
<pre class="prettyprint">

public class ManipularPedido {

	public List&lt;Pedido&gt; liberarPedido(List&lt;Pedido&gt; pedidos){
		for (Pedido pedido : pedidos) {
			pedido.setSituacaoPedido(SituacaoPedido.Liberado);
		}
		return pedidos;
	}
	
	public List&lt;Pedido&gt; liberarPedido(Pedido pedido) {
		
		List&lt;Pedido&gt; pedidos = new ArrayList&lt;Pedido&gt;();
		pedido.setSituacaoPedido(SituacaoPedido.Liberado);
		pedidos.add(pedido);
		return pedidos;
	}
}

</pre>

<script src="prettify/prettify_run.js"></script>