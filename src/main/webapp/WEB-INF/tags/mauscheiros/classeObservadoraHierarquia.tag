<%@ tag language="java" pageEncoding="ISO-8859-1"%>


<div style="width:720px;height:540px;">

<?prettify?>
<pre class="prettyprint">
public class Livro {

	private NotificarReserva notificacao = new NotificarReserva();
	...
	
	public void devolverLivro(){
		notificacao.notifica(this);
		setEstadoLivro(EstadoLivro.liberado);
	}
}
</pre>

<script src="prettify/prettify_run.js"></script>