<%@ tag language="java" pageEncoding="ISO-8859-1"%>


<div style="width:720px;height:540px;">

<?prettify?>
<pre class="prettyprint">

public Class Funcionario {

...

public Double calcularSalarioComComissao(){
		
		if (TipoFuncionario.GERENTE.equals(tipoFuncionario)){
			return getSalario() + (getSalario() * 0.3) + adicionalGerente();
		} // adiciona 30% ao sal�rio + 5% ao ano  
		else if (TipoFuncionario.COORDENADOR.equals(tipoFuncionario)){
			return getSalario() + (getSalario() * 0.2);
		} // adiciona 20% ao sal�rio 
		else {
			return getSalario() + (getSalario() * 0.1);
		} // adiciona 10% ao sal�rio
	}
}
	
</pre>

<script src="prettify/prettify_run.js"></script>