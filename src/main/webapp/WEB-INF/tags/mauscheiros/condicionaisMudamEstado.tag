<%@ tag language="java" pageEncoding="ISO-8859-1"%>


<div style="width:720px;height:540px;">

<?prettify?>
<pre class="prettyprint">
public class AlunoGraduacao {
...
	public void calcularAprovacaoAluno(){
		if (faltas &lt; 19){
			if (media() &gt;= 6.0 ){
				estadoAluno = EstadoAluno.Aprovado;
			}
			if (media() &gt;= 5.0 && media() &lt; 6.0 ){
				estadoAluno = EstadoAluno.ProvaFinal;
			}
			if (media() &lt; 5.0){
				estadoAluno =  EstadoAluno.Reprovado;
			}
		} else {
			estadoAluno =  EstadoAluno.Reprovado;
		}
	}
}
</pre>

<script src="prettify/prettify_run.js"></script>