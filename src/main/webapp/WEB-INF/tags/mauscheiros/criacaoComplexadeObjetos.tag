<%@ tag language="java" pageEncoding="ISO-8859-1"%>


<div style="width:720px;height:540px;">

<?prettify?>
<pre class="prettyprint">

public class TestConexao {
	
	@Test
	public void testarConexao(){
		ConfiguraConexao configuraConexao = 
				new ConfiguraConexao("admin","abcd01",TipoBaseDados.PRODUCAO);
		
		TipoConexao tipoConexao = new TipoConexao(configuraConexao);
		
		Conector conector = new Conector(tipoConexao);
		
		Conexao conexao = conector.criarConexao();
		
		Assert.assertTrue(conexao != null);
	}
}
</pre>

<script src="prettify/prettify_run.js"></script>