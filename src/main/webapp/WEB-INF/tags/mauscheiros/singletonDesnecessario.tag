<%@ tag language="java" pageEncoding="ISO-8859-1"%>


<div style="width:720px;height:540px;">

<?prettify?>
<pre class="prettyprint">

public class EquacaoSegundoGrau {

	private double a;
	private double b;
	private double c;
	 ...
	public Double valorDelta(){
		return CalculosEquacaoSegundoGrau.
				getInstancia().calcularDelta(a, b, c);
	}
	
}

</pre>

<script src="prettify/prettify_run.js"></script>