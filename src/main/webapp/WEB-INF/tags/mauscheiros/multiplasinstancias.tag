<%@ tag language="java" pageEncoding="ISO-8859-1"%>


<div style="width:720px;height:540px;">

<?prettify?>
<pre class="prettyprint">


public class TestarConexao {

	@Test
	public void TestarDuasInstanciasBD(){
		DataBaseConnection conexaoBD1 = new DataBaseConnection();
		
		conexaoBD1.conectarBase();
		
		conexaoBD1.recuperarRegistros();
		
		conexaoBD1.desconectarBase();
		
		DataBaseConnection conexaoBD2 = new DataBaseConnection();
		
		conexaoBD2.conectarBase();
		
		conexaoBD2.recuperarRegistros();
		
		conexaoBD2.desconectarBase();
		
		Assert.assertTrue(!conexaoBD1.equals(conexaoBD2));
	}
	
}
	
</pre>

<script src="prettify/prettify_run.js"></script>