<%@ tag language="java" pageEncoding="ISO-8859-1"%>


<div style="width:720px;height:540px;">

<?prettify?>
<pre class="prettyprint">

public class TesteCursoPreVestibular {

	Curso curso = null;
	
	@Test
	public void testarCursoIntensivo(){
		curso = new CursoIntensivo();
		String descricaoCurso = 
		curso.infoCurso()
		.concat(curso.prepararAula())
		.concat(curso.avaliacaoSemanal());
		
		Assert.assertTrue(descricaoCurso.
				contains("Intensivo"));
	}
	@Test
	public void testarCursoExtensivo(){
		curso = new CursoExtensivo();
		String descricaoCurso = 
		curso.infoCurso()
		.concat(curso.prepararAula())
		.concat(curso.avaliacaoSemanal());
		
		Assert.assertTrue(descricaoCurso.
				contains("Extensivo"));
	}
}

	
</pre>

<script src="prettify/prettify_run.js"></script>