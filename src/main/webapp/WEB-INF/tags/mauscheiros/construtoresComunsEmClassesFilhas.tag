<%@ tag language="java" pageEncoding="ISO-8859-1"%>


<div style="width:720px;height:540px;">

<?prettify?>
<pre class="prettyprint">
public class TestarWebDriver {
	@Test
	public void testarDriverChrome(){
		
		TestDriverChrome driver = new TestDriverChrome();
		
		String browser = driver.direcionarURL("http://www.google.com");
		
		Assert.assertTrue(browser.contains("Chrome"));
	}
	
	@Test
	public void testarDriverFirefox(){
		
		TestDriverFirefox driver = new TestDriverFirefox();
		
		String browser = driver.direcionarURL("http://www.google.com");
		
		Assert.assertTrue(browser.contains("Firefox"));
	}
}
</pre>

<script src="prettify/prettify_run.js"></script>