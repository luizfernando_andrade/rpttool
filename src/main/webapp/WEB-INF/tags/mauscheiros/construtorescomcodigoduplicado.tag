<%@ tag language="java" pageEncoding="ISO-8859-1"%>


<div style="width:720px;height:540px;">

<?prettify?>
<pre class="prettyprint">
public class Funcionario {
...
	public Funcionario( String nome, String cpf, Date nascimento, 
			List&lt;Dependente&gt; dependentes, Boolean possuiCNH) {
		this.nome = nome;
		this.cpf = cpf;
		this.nascimento = nascimento;
		this.dependentes = dependentes;
		this.possuiCNH = possuiCNH;
	}
	public Funcionario( String nome, String cpf, Date nascimento, 
			List&lt;Dependente&gt; dependentes) {
		this.nome = nome;
		this.cpf = cpf;
		this.nascimento = nascimento;
		this.dependentes = dependentes;
	}
	public Funcionario( String nome, String cpf, Date nascimento) {
		this.nome = nome;
		this.cpf = cpf;
		this.nascimento = nascimento;
	}
	public Funcionario( String nome, String cpf, Date nascimento, 
			Boolean possuiCNH) {
		this.nome = nome;
		this.cpf = cpf;
		this.nascimento = nascimento;
		this.possuiCNH = possuiCNH;
	}
...
}
</pre>

<script src="prettify/prettify_run.js"></script>