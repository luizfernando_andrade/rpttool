<%@ tag language="java" pageEncoding="ISO-8859-1"%>


<div style="width:720px;height:540px;">

<?prettify?>
<pre class="prettyprint">
public class Encomenda {

	private String situacao;
	private static final String SEPARACAO = "Separacao";
	private static final String TRANSPORTADORA = "Transportadora";
	private static final String CORREIOS = "Correios";
	private static final String ENTREGUE = "Entregue";
	
	public String getSituacao() {
		return situacao;
	}
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	public String mudarSituacao(){
		if (situacao.equals(SEPARACAO)){
			situacao = TRANSPORTADORA;
		} else if (situacao.equals(TRANSPORTADORA)) {
			situacao = CORREIOS;
		} else {
			situacao = ENTREGUE;
		}
		return situacao;
	}	
}


</pre>

<script src="prettify/prettify_run.js"></script>