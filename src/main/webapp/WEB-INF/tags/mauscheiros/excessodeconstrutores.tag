<%@ tag language="java" pageEncoding="ISO-8859-1"%>


<div style="width:720px;height:540px;">

<?prettify?>
<pre class="prettyprint">

public Class Aluno {
...
	public Aluno(String nome, Date nascimento, Integer matricula){
		this.nome = nome;
		this.nascimento = nascimento;
		this.matricula = matricula;
	}
	public Aluno(String nome, Date nascimento, Integer matricula,Turma turma){
		this.nome = nome;
		this.nascimento = nascimento;
		this.matricula = matricula;
		this.turma = turma;
	}
	public Aluno(String nome, Date nascimento, Integer matricula,
	                          Turma turma, Dependencia dependencia){
		this.nome = nome;
		this.nascimento = nascimento;
		this.matricula = matricula;
		this.turma = turma;
		this.dependencia = dependencia;
	}
	public Aluno(String nome, Date nascimento, Integer matricula, Dependencia dependencia){
		this.nome = nome;
		this.nascimento = nascimento;
		this.matricula = matricula;
		this.dependencia = dependencia;
	}
	
}
	
</pre>

<script src="prettify/prettify_run.js"></script>