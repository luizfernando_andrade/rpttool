<%@ tag language="java" pageEncoding="ISO-8859-1"%>


<div style="width:720px;height:540px;">

<?prettify?>
<pre class="prettyprint">
public class Expressao {

	HashMap&lt;String, Double&gt; eixoX = new HashMap&lt;&gt;();
	
	public Map&lt;String, Double&gt; calcularBhaskara(Double a, Double b, Double c){
		
		eixoX.put("x1", (-b + Math.sqrt( Math.pow(b, 2) - (4*a*c)))/2*a);
		eixoX.put("x2", (-b - Math.sqrt( Math.pow(b, 2) - (4*a*c)))/2*a);
		
		return eixoX;
	}
	
}
</pre>

<script src="prettify/prettify_run.js"></script>