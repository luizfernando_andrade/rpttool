<%@ tag language="java" pageEncoding="ISO-8859-1"%>


<div style="width:720px;height:540px;">

<?prettify?>
<pre class="prettyprint">
public class ControlePostoGasolina {
...
	public String validarArquivo(){
		
		if (TipoArquivo.Financeiro.equals(arquivo.getTipoArquivo())){
			ArquivoFinanceiro financeiro = new ArquivoFinanceiro();
			financeiro.adicionarValor(arquivo.getImpostosPagos());
			arquivo.setResponsavel(financeiro.setorResponsavel());
			return arquivo.getResponsavel();
		}
		if (TipoArquivo.Comercial.equals(arquivo.getTipoArquivo())){
			ArquivoComercial comercial = new ArquivoComercial();
			comercial.adicionarValor(arquivo.getImpostosPagos());
			arquivo.setResponsavel(comercial.setorResponsavel());
			return arquivo.getResponsavel();
		} else {
			ArquivoEstoque estoque= new ArquivoEstoque();
			estoque.adicionarValor(arquivo.getImpostosPagos());
			arquivo.setResponsavel(estoque.setorResponsavel());
			return arquivo.getResponsavel();
		}
	}
}
</pre>

<script src="prettify/prettify_run.js"></script>