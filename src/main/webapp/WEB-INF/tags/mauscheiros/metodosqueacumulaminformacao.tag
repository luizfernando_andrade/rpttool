<%@ tag language="java" pageEncoding="ISO-8859-1"%>


<div style="width:720px;height:540px;">

<?prettify?>
<pre class="prettyprint">
public class RelatorioEnfermaria implements Relatorio {

	public String imprimir(TipoArquivo tipoArquivo){
		StringBuffer sb = new StringBuffer();
		String resultado = null;
		
		if (tipoArquivo instanceof TipoArquivoHTML){

			sb.append("&lt;head&gt;" + getCabecalho() + "&lt;/head&gt;");
			sb.append("&lt;body&gt;" + getCorpo() + "&lt;/body&gt;");
			sb.append("&lt;footer&gt;" + getRodape() + "&lt;/footer&gt;");
			resultado = tipoArquivo.imprimir(sb.toString());
		}
		if (tipoArquivo instanceof TipoArquivoXML) {
			
			sb.append("&lt;cabecalho&gt;" + getCabecalho() + "&lt;/cabecalho&gt;");
			sb.append("&lt;corpo&gt;" + getCorpo() + "&lt;/corpo&gt;");
			sb.append("&lt;rodape&gt;" + getRodape() + "&lt;/rodape&gt;");
			resultado = tipoArquivo.imprimir(sb.toString());
		}
		return resultado; 
	}
</pre>

<script src="prettify/prettify_run.js"></script>