<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<nav class="navbar navbar-default">
	<div class="container-fluid">
		<div class="collapse navbar-collapse"
			id="bs-example-navbar-collapse-1">
			<ul class="nav nav-pills">
				<li role="presentation"><a href="index.jsp">Inicial</a></li>
				<li class="dropdown"><a class="dropdown-toggle"
					data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
					aria-expanded="false"> Tipo Problema <span class="caret"></span>
				</a>
					<ul class="dropdown-menu">
						<li><a href="filtrarProblemaController?tipoProblema=1">Criação</a></li>
						<li><a href="filtrarProblemaController?tipoProblema=2">Simplificação</a></li>
						<li><a href="filtrarProblemaController?tipoProblema=3">Generalização</a></li>
						<li><a href="filtrarProblemaController?tipoProblema=4">Proteção</a></li>
						<li><a href="filtrarProblemaController?tipoProblema=5">Acumulação</a></li>
						<li><a href="filtrarProblemaController?tipoProblema=6">Utilitários</a></li>
					</ul></li>
				<!-- 	<li class="dropdown"> -->
				<!-- 	<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button"  -->
				<!-- 	aria-haspopup="true" aria-expanded="false"> Tipo Refatoração <span class="caret"></span> -->
				<!-- 	</a> -->
				<!-- 		<ul class="dropdown-menu"> -->
				<!-- 			<li><a href="#">Criação</a></li> -->
				<!-- 			<li><a href="#">Simplificação</a></li> -->
				<!-- 			<li><a href="#">Generalização</a></li> -->
				<!-- 			<li><a href="#">Proteção</a></li> -->
				<!-- 			<li><a href="#">Acumulação</a></li> -->
				<!-- 			<li><a href="#">Utilitários</a></li> -->
				<!-- 		</ul> -->
				<!-- 	</li> -->
<!-- 				<li role="presentation"><a href="index.jsp">Questionário -->
<!-- 						reação</a></li> -->
			</ul>
		</div>
	</div>
</nav>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.js"></script>



