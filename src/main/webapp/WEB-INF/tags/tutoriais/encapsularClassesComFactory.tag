<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@taglib tagdir="/WEB-INF/tags/mauscheiros" prefix="mc"%>
<div style="margin-left: 25px;">
	<h2>Tutorial Refatora��o</h2>

	<h3>Encapsular Classes com Factory</h3>

	<p>Caso queria acompanhar esse tutorial usando o mesmo contexo
		basta clicar no link, fazer o download do projeto e buscar a
		refatora��o deste tutorial.</p>

	<a
		href="https://luizfernando_andrade@bitbucket.org/luizfernando_andrade/torefactor.git">
		projeto_para_download</a> <br> <br> <br>

	<p>Passo 1 - Identificar construtor da classes de mesma hierarquia
		que s�o chamados de forma direta, e criar um m�todo de cria��o. O
		m�todo deve ser est�tico e publico.</p>
	<br>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public class ComponenteEletronico extends Componente {

	public ComponenteEletronico(String nome) {
		super(nome);
	}
	@Override
	protected String tipoComponente() {
		return "Componente Eletr�nico";
	}
	public static ComponenteEletronico criarComponenteEletronico(String nome){
		return new ComponenteEletronico(nome);
	}

}
		</pre>
	</div>
	<br>
	<p>Passo 2 - Aplicar refatora��o, subir m�todo para classe pai.
		Desta forma a classe pai da hierarquia deve ficar da seguinte forma.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public abstract class Componente {

...
	public static ComponenteEletronico criarComponenteEletronico(String nome) {
		return new ComponenteEletronico(nome);
	}

...
}
		</pre>
	</div>


	<br>
	<p>Passo 3 - Substituir a chamada do construtor da classe pelo novo
		m�todo de cria��o e aplicar o teste para validar a sa�da.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
		
public class TestarMontagemEquipamento {

	@Test
	public void testarMontagem(){
		MontarEquipamento equipamento = new MontarEquipamento();
		List&lt;Componente&gt; componentes = new ArrayList&lt;&gt;();
		
		componentes.add(Componente.criarComponenteEletronico("fuz�vel"));
		componentes.add(new ComponenteMecanico("Alavanca disjuntor"));
		componentes.add(new ComponentePeriferico("Cabo de alta tens�o"));
		
		equipamento.setDescricao("Painel el�trico");
		equipamento.setComponentes(componentes);
		
		Assert.assertTrue(equipamento.getComponentes() != null
						  && equipamento.getComponentes().size() == 3);
	}
}	
		
		</pre>
	</div>

	<br>
	<p>Passo 4 - Aplicar passos 1 e 2 nas demais classes da hierarquia.
		Ap�s altera��o a classe Componente deve ficar da seguinte forma.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public abstract class Componente {
	...
	public static ComponenteEletronico criarComponenteEletronico(String nome) {
		return new ComponenteEletronico(nome);
	}
	public static Componente criarComponenteMecanico(String nome) {
		return new ComponenteMecanico(nome);
	}
	public static Componente criarComponentePeriferico(String nome) {
		return new ComponentePeriferico(nome);
	}
	...
}
		</pre>
	</div>

	<br>
	<p>Passo 5 - Os construtores das classes filhas da hierarquia devem ter visibilidade 
	limitada, � necess�rio alterar a visibilidade para protected.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public class ComponentePeriferico extends Componente {
...
	protected ComponentePeriferico(String nome) {
		super(nome);
	}
}
public class ComponenteMecanico extends Componente {
...
	protected ComponenteMecanico(String nome) {
		super(nome);
	}
}
public class ComponenteEletronico extends Componente {
...
	protected ComponenteEletronico(String nome) {
		super(nome);
	}
}
		</pre>
	</div>

<br>
	<p>Passo 6 - Ap�s as altera��es � necess�rio ajustar a classe de
		teste para implementar a chamada do m�todo de cria��o nas demais
		instancia��es e validar a sa�da</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public class TestarMontagemEquipamento {

	@Test
	public void testarMontagem(){
		MontarEquipamento equipamento = new MontarEquipamento();
		List&lt;Componente&gt; componentes = new ArrayList&lt;&gt;();
		
		componentes.add(Componente.criarComponenteEletronico("fuz�vel"));
		componentes.add(Componente.criarComponenteMecanico("Alavanca disjuntor"));
		componentes.add(Componente.criarComponentePeriferico("Cabo de alta tens�o"));
		
		equipamento.setDescricao("Painel el�trico");
		equipamento.setComponentes(componentes);
		
		Assert.assertTrue(equipamento.getComponentes() != null
						  && equipamento.getComponentes().size() == 3);
	}
}
		</pre>
	</div>
	<!-- Button trigger modal -->
	<button type="button" class="btn btn-primary btn-lg"
		data-toggle="modal" data-target="#myModal"
		style="position: fixed; bottom: 50px; right: 50px;">Exibir
		Problema</button>

	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<mc:reduzirVisibilidadeDeInstancias/>
			<div class="modal-content">
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="prettify/prettify_run.js"></script>