<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@taglib tagdir="/WEB-INF/tags/mauscheiros" prefix="mc"%>
<div style="margin-left: 25px;">
	<h2>Tutorial Refatora��o</h2>
	
	<h3>Mover Embelezamento para Decorator</h3>
	
	<p>Caso queria acompanhar esse tutorial usando o mesmo contexo basta clicar no link, fazer o download do 
		projeto e buscar a refatora��o deste tutorial. </p>
	
	<a href="https://luizfernando_andrade@bitbucket.org/luizfernando_andrade/torefactor.git">
	projeto_para_download</a>
	<br><br><br>

	<p>Passo 1 - Encapsular a classe embelezada pelos m�todos adicionais com uma nova classe, 
		o Decorator. A classe de possuir acesso aos componentes da classe decorada.</p>
	<br>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
		
package refatoracao.decorator.pojo;

public class AulaExtraDecorator 
			extends CursoIntensivo{

	public AulaExtraDecorator(){}
	
}

		</pre>
	</div>
	<br>
	<p>Passo 2 - No construtor da classe decorator delegue as atribui��es � classe. 
		crie os atributos necess�rios. Ap�s altera��o a classe deve ficar como segue.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
		
public class AulaExtraDecorator 
			extends CursoIntensivo{

	private String diaAulaExtra2;
	private CursoIntensivo curso;

	public AulaExtraDecorator(String diaAulaExtra, CursoIntensivo Curso){
		diaAulaExtra2 = diaAulaExtra;
		curso = Curso;
	}
}
		</pre>
	</div>
	
	
	<br>
	<p>Passo 3 - Os novos m�todos devem ser implementados nesta classe.
	no caso os m�todos assinalados como adicionais na classe.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public class AulaExtraDecorator 
			extends CursoIntensivo{

	private String diaAulaExtra;
	private CursoIntensivo curso;

	public AulaExtraDecorator(String diaAulaExtra, CursoIntensivo Curso){
		this.diaAulaExtra = diaAulaExtra;
		curso = Curso;
	}
	//m�todos adicionais
	public String infoBasica(){
		return tipoCurso() + " " + getDiaAulaExtra();
	}
	public String getDiaAulaExtra() {
		return diaAulaExtra;
	}
	public void setDiaAulaExtra(String diaAulaExtra) {
		this.diaAulaExtra = diaAulaExtra;
	}
}

		</pre>
	</div>
	
	
	<br>

	<p>Passo 4 - O teste parou de executar pois foi implementado usando a forma antiga da classe,
		A seguir � mostrado o teste que dever� ser alterado.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public class TestarCurso {

	@Test
	public void testarCursoIntensivo(){
		String aulaExtra = "sabado";
		
		CursoIntensivo cursoIntensivo = new CursoIntensivo();
		
		cursoIntensivo.aulaExtra(aulaExtra);
		
		String informacaoCurso = cursoIntensivo.infoBasica();
		
		Assert.assertTrue(informacaoCurso.contains("extra"));
	}
}
	
		</pre>
	</div>
	
	<br>
	<p>Passo 5 - Alterar as chamadas dos m�todos adicionais para usarem o decorator.
	O novo teste deve ficar como segue.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
		
public class TestarCurso {

	@Test
	public void testarCursoIntensivo(){
		String aulaExtra = "sabado";
		
		CursoIntensivo cursoIntensivo = new CursoIntensivo();
		
		AulaExtraDecorator cursoDecorado = new AulaExtraDecorator(aulaExtra, cursoIntensivo);
		
		String informacaoCurso = cursoDecorado.infoBasica();
		
		Assert.assertTrue(informacaoCurso.contains("extra"));
	}
}

		</pre>
	</div>	
	
	
	<!-- Button trigger modal -->
<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal"
style="position:fixed;bottom: 50px;right: 50px;">
  Exibir Problema
</button>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
      <mc:excessoResponsabilidadeClasse/>
    <div class="modal-content">
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>
</div>
<script src="prettify/prettify_run.js"></script>