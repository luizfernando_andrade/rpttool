<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@taglib tagdir="/WEB-INF/tags/mauscheiros" prefix="mc"%>
<div style="margin-left: 25px;">
	<h2>Tutorial Refatora��o</h2>

	<h3>Mover Conhecimento de Cria��o para Factory</h3>

	<p>Caso queria acompanhar esse tutorial usando o mesmo contexo
		basta clicar no link, fazer o download do projeto e buscar a
		refatora��o deste tutorial.</p>

	<a
		href="https://luizfernando_andrade@bitbucket.org/luizfernando_andrade/torefactor.git">
		projeto_para_download</a> <br> <br> <br>

	<p>Passo 1 - Identificar onde � criado o objeto resultante e
		verificar se sua cria��o � a partir de um m�todo de cria��o. Caso n�o
		seja, centralizar a cria��o em um m�todo de cria��o. Neste caso o
		metodo de cria��o est� na classe Conector</p>
	<br>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">

Public class Conector {
...
	public Conexao criarConexao(){
		if (tipoConexao.getConfiguraConexao().getBaseDados()
				.equals(TipoBaseDados.CERTIFICACAO)){
			return new Conexao("Conectado a: certificacao/aplicacao");
		} else if (tipoConexao.getConfiguraConexao().getBaseDados()
				.equals(TipoBaseDados.HOMOLOGACAO)){
			return new Conexao("Conectado a: homologacao/aplicacao");
		} else if (tipoConexao.getConfiguraConexao().getBaseDados()
				.equals(TipoBaseDados.PRODUCAO)){
			return new Conexao("Conectado a: producao/aplicacao");
		} else {
			return new Conexao("Conectado a: localhost/aplicacao");
		}
	}
	...
}
		</pre>
	</div>
	<br>
	<p>Passo 2 - Criar uma classe factory que armazenar� o conhecimento
		de cria��o</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
package refatoracao.conector.pojo;

public class ConexaoFactory {

}

		</pre>
	</div>


	<br>
	<p>Passo 3 - Mover o metodo de cria��o para a nova classe factory.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public class ConexaoFactory {
...
	public Conexao criarConexao(){
		if (tipoConexao.getConfiguraConexao().getBaseDados()
				.equals(TipoBaseDados.CERTIFICACAO)){
			return new Conexao("Conectado a: certificacao/aplicacao");
		} else if (tipoConexao.getConfiguraConexao().getBaseDados()
				.equals(TipoBaseDados.HOMOLOGACAO)){
			return new Conexao("Conectado a: homologacao/aplicacao");
		} else if (tipoConexao.getConfiguraConexao().getBaseDados()
				.equals(TipoBaseDados.PRODUCAO)){
			return new Conexao("Conectado a: producao/aplicacao");
		} else {
			return new Conexao("Conectado a: localhost/aplicacao");
		}
	}
...	
}	
		</pre>
	</div>

	<br>
	<p>Passo 4 - Mover todo conhecimento necess�rio para a cria��o do
		objeto para a nova classe, neste caso foi passado o objeto 
		configuraConexao como atributo da classe para que ser adicionado em 
		tempo de execu��o. A classe factory ficou da seguinte forma. </p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
	
package refatoracao.conector.pojo;

public abstract class ConexaoFactory {
	
	ConfiguraConexao configuraConexao; 
	
	abstract void adicionarTipoConexao(TipoConexao tipoConexao);
	
	public Conexao criarConexao(){
		
		TipoConexao tipoConexao = new TipoConexao(configuraConexao);
		
		adicionarTipoConexao(tipoConexao);
		
		if (tipoConexao.getConfiguraConexao().getBaseDados()
				.equals(TipoBaseDados.CERTIFICACAO)){
			return new Conexao("Conectado a: certificacao/aplicacao");
		} else if (tipoConexao.getConfiguraConexao().getBaseDados()
				.equals(TipoBaseDados.HOMOLOGACAO)){
			return new Conexao("Conectado a: homologacao/aplicacao");
		} else if (tipoConexao.getConfiguraConexao().getBaseDados()
				.equals(TipoBaseDados.PRODUCAO)){
			return new Conexao("Conectado a: producao/aplicacao");
		} else {
			return new Conexao("Conectado a: localhost/aplicacao");
		}
	}

	public ConfiguraConexao getConfiguraConexao() {
		return configuraConexao;
	}

	public void setConfiguraConexao(ConfiguraConexao configuraConexao) {
		this.configuraConexao = configuraConexao;
	}
	
}

		
		</pre>
	</div>
	
	
	<br>
	<p>Passo 5 - Foi criado o metodo na classe conector para que fosse adicionado o 
	tipo de conex�o referente � configura��o informada. A classe conector possui o
	m�todo a seguir</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public class Conector extends ConexaoFactory {
	
	TipoConexao tipoConexao;
...
	@Override
	void adicionarTipoConexao(TipoConexao tipoConexao) {
		this.tipoConexao = tipoConexao;
		
	}
...
}

		</pre>
	</div>
	
	<br>
	<p>Passo 6 - � necess�rio ajustar o contexto para seguir o novo
		modelo de cria��o do objeto, neste caso � s� alterar a classe de teste
		para que fique do seguinte modo.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public class TesteConexaoBD {

	@Test
	public void TestarConexaoCertificacao(){
		ConfiguraConexao configuraConexao = new ConfiguraConexao();
		
		configuraConexao.setUsuario("root");
		configuraConexao.setSenha("12345678909");
		configuraConexao.setBaseDados(TipoBaseDados.CERTIFICACAO);
		
		
		Conector conector = new Conector();
		
		conector.setConfiguraConexao(configuraConexao);
		
		conector.setTipoConector(TipoConector.MSQL);
		
		Conexao conexao = conector.criarConexao();
		
		Assert.assertTrue(conexao.getUrlConexao().contains("cert"));
	}
}
		</pre>
	</div>
	
	
	<!-- Button trigger modal -->
	<button type="button" class="btn btn-primary btn-lg"
		data-toggle="modal" data-target="#myModal"
		style="position: fixed; bottom: 50px; right: 50px;">Exibir
		Problema</button>

	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<mc:espalhamentoCriacao/>
			<div class="modal-content">
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="prettify/prettify_run.js"></script>