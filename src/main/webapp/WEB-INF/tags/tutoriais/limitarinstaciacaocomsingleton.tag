<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@taglib tagdir="/WEB-INF/tags/mauscheiros" prefix="mc"%>
<div style="margin-left: 25px;">
	<h2>Tutorial Refatora��o</h2>

	<h3>Limitar Instancia��o com Singleton</h3>

	<p>Caso queria acompanhar esse tutorial usando o mesmo contexo
		basta clicar no link, fazer o download do projeto e buscar a
		refatora��o deste tutorial.</p>

	<a
		href="https://luizfernando_andrade@bitbucket.org/luizfernando_andrade/torefactor.git">
		projeto_para_download</a> <br> <br> <br>

	<p>Passo 1 - Identificar a classe que deve possuir instancia �nica
		por�m possui v�rios objetos espalhados pelo c�digo. Torne o construtor
		desta classe privado.</p>
	<br>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">

public class DataBaseConnection {
...
	private DataBaseConnection() {}
...
	
}
		</pre>
	</div>
	<br>
	<p>Passo 2 - Criar na classe uma vari�vel est�tica do pr�prio tipo
		da classe sendo incializada pelo construtor criado.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">


public class DataBaseConnection {
	
	private static DataBaseConnection dbConnection = new DataBaseConnection();

	private DataBaseConnection() {}
	
	...

}
		</pre>
	</div>


	<br>
	<p>Passo 3 - Implementar um m�todo que retorne uma inst�ncia da
		classe caso ainda n�o exista. O m�todo deve ser est�tico para
		manipular a vari�vel de instancia tamb�m est�tica</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
		
public class DataBaseConnection {
	
	private static DataBaseConnection dbConnection = new DataBaseConnection();

	private DataBaseConnection() {}
	
	public static DataBaseConnection getInstancia() {
		if (dbConnection == null){
			dbConnection = new DataBaseConnection();
		}
		return dbConnection;		
	}
...
}
	
		</pre>
	</div>


	<br>

	<p>Passo 4 - Substituir os locais onde eram usados os contrutores para
		criar novas instancias, pelo m�todo que recupera a instancia �nica da
		classe. Foi necess�rio alterar o "Assert" do teste pois agora as
		vari�veis possuem a mesma instancia da classe</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
		
public class TestarConexao {

	@Test
	public void TestarDuasInstanciasBD(){
		DataBaseConnection conexaoBD1 = DataBaseConnection.getInstancia();
		
		conexaoBD1.conectarBase();
		
		conexaoBD1.recuperarRegistros();
		
		conexaoBD1.desconectarBase();
		
		DataBaseConnection conexaoBD2 = DataBaseConnection.getInstancia();
		
		conexaoBD2.conectarBase();
		
		conexaoBD2.recuperarRegistros();
		
		conexaoBD2.desconectarBase();
		
		Assert.assertTrue(conexaoBD1.equals(conexaoBD2));
	}
}
	
		</pre>
	</div>


	<!-- Button trigger modal -->
	<button type="button" class="btn btn-primary btn-lg"
		data-toggle="modal" data-target="#myModal"
		style="position: fixed; bottom: 50px; right: 50px;">Exibir
		Problema</button>

	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<mc:multiplasinstancias/>
			<div class="modal-content">
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="prettify/prettify_run.js"></script>