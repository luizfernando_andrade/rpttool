<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@taglib tagdir="/WEB-INF/tags/mauscheiros" prefix="mc"%>
<div style="margin-left: 25px;">
	<h2>Tutorial Refatora��o</h2>

	<h3>Substituir Linguagem impl�cita por Interpreter</h3>

	<p>Caso queria acompanhar esse tutorial usando o mesmo contexo
		basta clicar no link, fazer o download do projeto e buscar a
		refatora��o deste tutorial.</p>

	<a
		href="https://luizfernando_andrade@bitbucket.org/luizfernando_andrade/torefactor.git">
		projeto_para_download</a> <br> <br> <br>

	<p>Passo 1 - Criar as classes que representaram as express�es
		matem�ticas, classe Numero, classe Soma e classe Subtracao.</p>
	<br>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
		
//Classe que represetaram a express�o
public class Subtracao {

}
public class Soma {

}
public class Numero {

}

		</pre>
	</div>
	<br>
	<p>Passo 2 - Remover os atributos da classe Expressao e converte-la
		em interface.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">

package refatoracao.expressao.pojo;

public interface Expressao {

	
}

		</pre>
	</div>


	<br>
	<p>Passo 3 - Assinar o m�todo avaliarExpressao na interface rec�m
		criada</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
		
package refatoracao.expressao.pojo;

public interface Expressao {

	public int avaliarExpressao();
	
}

		
		</pre>
	</div>

	<br>
	<p>Passo 4 - Na classe n�mero implementar a nova interface e
		adicionar o m�todo necess�rio.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public class Numero implements Expressao {
	
	@Override
	public int avaliarExpressao() {
		return 0;
	}

}
		</pre>
	</div>


	<br>
	<p>Passo 5 - Adicionar o atributo numero do tipo inteiro e
		retorn�-lo no m�todo avaliar, deve ser recebido no contrutor.</p>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public class Numero implements Expressao {

	private int numero;

	public Numero(int numero) {
		this.numero = numero;
	}
	
	@Override
	public int avaliarExpressao() {
		return numero;
	}

}

		</pre>
	</div>

	<br>
	<p>Passo 6 - Na classe Soma implementar a interface a adicionar o
		m�todo avaliarExpressao.</p>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">

public class Soma implements Expressao{

	@Override
	public int avaliarExpressao() {
		// TODO Auto-generated method stub
		return 0;
	}

}
		</pre>
	</div>


	<br>
	<p>Passo 7 - Adicionar os atributos expressaoEsquerda e
		espressaoDireita e receb�-los via contrutor.</p>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">

public class Soma implements Expressao{

	private Expressao esquerda;
	private Expressao direita;
	public Soma(Expressao esquerda, Expressao direita) {
		this.esquerda = esquerda;
		this.direita = direita;
	}
	@Override
	public int avaliarExpressao() {
		// TODO Auto-generated method stub
		return 0;
	}

}

		</pre>
	</div>
	<br>
	<p>Passo 8 - Implementar no m�todo avaliarExpressao a soma do
		resultado das avalia��es, como exemplo apresentado.</p>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">

public class Soma implements Expressao{

	private Expressao esquerda;
	private Expressao direita;
	public Soma(Expressao esquerda, Expressao direita) {
		this.esquerda = esquerda;
		this.direita = direita;
	}
	@Override
	public int avaliarExpressao() {
		return esquerda.avaliarExpressao()
				+ direita.avaliarExpressao();
	}

}


		</pre>
	</div>

	<br>

	<p>Passo 9 - repetir passos de 6 a 8 para classe Subtracao por�m
		invertendo o sinal matem�tico.</p>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
		
public class Subtracao implements Expressao {

	private Expressao direita;
	private Expressao esqueda;
	public Subtracao(Expressao direita, Expressao esqueda) {
		this.direita = direita;
		this.esqueda = esqueda;
	}
	@Override
	public int avaliarExpressao() {
		return direita.avaliarExpressao() 
				- esqueda.avaliarExpressao();
	}

}


		</pre>
	</div>
	<br>

	<p>Passo 10 - Ajustar classe de teste para usar nova estrutura de
		interpreta��o.</p>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
		
public class TestarCalculadora {

	@Test
	public void testarSoma(){
		
		Expressao soma = new Soma(new Numero(10), new Numero(7));
		int resultado = soma.avaliarExpressao();
		
		Assert.assertTrue(resultado == 17);
	}
}


		</pre>
	</div>




	<!-- Button trigger modal -->
	<button type="button" class="btn btn-primary btn-lg"
		data-toggle="modal" data-target="#myModal"
		style="position: fixed; bottom: 50px; right: 50px;">Exibir
		Problema</button>

	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<mc:metodosComElementosLiguagemImplicita/>
			<div class="modal-content">
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="prettify/prettify_run.js"></script>