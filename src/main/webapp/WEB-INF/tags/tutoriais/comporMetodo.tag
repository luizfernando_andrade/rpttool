<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@taglib tagdir="/WEB-INF/tags/mauscheiros" prefix="mc"%>
<div style="margin-left: 25px;">
	<h2>Tutorial Refatora��o</h2>

	<h3>Compor M�todo</h3>

	<p>Caso queria acompanhar esse tutorial usando o mesmo contexo
		basta clicar no link, fazer o download do projeto e buscar a
		refatora��o deste tutorial.</p>

	<a
		href="https://luizfernando_andrade@bitbucket.org/luizfernando_andrade/torefactor.git">
		projeto_para_download</a> <br>
	<br>
	<br>

	<p>Passo 1 - Identificar no m�todo responsabilidades que podem ser
		extraidas para novos m�todos. No exemplo s�o a verifica��o do
		condicional, a adi��o de carro nas vagas e a adi��o de carros na fila
		de espera. aplique a refatora��o mover m�todo na condi��o e adinione
		um nome sugestivo ao m�todo.</p>
	<br>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public class Garagem {
...
	public void validarEntrada(Carro carro){
		if (possuiVagas()){
			carrosEstacionados.add(carro);
			vagasOcupadas ++;
		} else {
			carrosEspera.add(carro);
			filaEspera++;
		}
	}
	
	private boolean possuiVagas() {
		return carrosEstacionados.size() &lt; numeroVagas;
	}
}
		</pre>
	</div>
	<br>
	<p>Passo 2 - executar o teste para validar a sa�da, que deve
		continuar a mesma.</p>
		
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public class TestarGaragem {

	@Test
	public void testarListaEspera(){
		
		List&lt;Carro&gt; carros = new ArrayList&lt;Carro&gt;();
		carros.add(new Carro("VW", "GOL"));
		carros.add(new Carro("Audi", "A3"));
		carros.add(new Carro("VW", "Saveiro"));
		carros.add(new Carro("Audi", "TT"));
		
		Garagem garagem = new Garagem();
		
		for (Carro carro : carros) {
			garagem.validarEntrada(carro);
		}
		System.out.println(garagem.getFilaEspera());
		Assert.assertTrue(garagem.getFilaEspera() == 1);
	}
}
		</pre>
	</div>


	<br>
	<p>Passo 3 - Realizar a mudan�a relacionada ao passo 1 onde houver
		necessidade e validar a saida com o teste. Ap�s as altera��es a classe
		deve ficar na forma como se segue. Fragmentar o m�todo deixa mais 
		f�cil o uso fora do dom�nio da classe, basta tornar a visibilidade publica. </p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
		
	public void validarEntrada(Carro carro){
		if (possuiVagas()){
			adicionarVaga(carro);
		} else {
			adicionarEspera(carro);
		}
	}
	private void adicionarEspera(Carro carro) {
		carrosEspera.add(carro);
		filaEspera++;
	}
	private void adicionarVaga(Carro carro) {
		carrosEstacionados.add(carro);
		vagasOcupadas ++;
	}
	private boolean possuiVagas() {
		return carrosEstacionados.size() &lt; numeroVagas;
	}
			
		</pre>
	</div>


	<!-- Button trigger modal -->
	<button type="button" class="btn btn-primary btn-lg"
		data-toggle="modal" data-target="#myModal"
		style="position: fixed; bottom: 50px; right: 50px;">Exibir
		Problema</button>

	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<mc:metodoComMuitasResponsabilidades/>
			<div class="modal-content">
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="prettify/prettify_run.js"></script>