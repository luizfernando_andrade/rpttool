<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@taglib tagdir="/WEB-INF/tags/mauscheiros" prefix="mc"%>
<div style="margin-left: 25px;">
	<h2>Tutorial Refatora��o</h2>

	<h3>Substituir condicionais que alteram estado por State</h3>

	<p>Caso queria acompanhar esse tutorial usando o mesmo contexo
		basta clicar no link, fazer o download do projeto e buscar a
		refatora��o deste tutorial.</p>

	<a
		href="https://luizfernando_andrade@bitbucket.org/luizfernando_andrade/torefactor.git">
		projeto_para_download</a> <br> <br> <br>

	<p>Passo 1 - identificar para quais estados s�o alterados os
		objetos, e criar uma interface State para a manipula��o dos estados.
		adicionar assinatura de m�todos que controlaram os estados.</p>
	<br>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
		
package refatoracao.secretaria.pojo;

public interface AlunoEstado {
	
	AlunoEstado aprovado();
	
	AlunoEstado reprovado();
	
	AlunoEstado provaFinal();

}

		</pre>
	</div>
	<br>
	<p>Passo 2 - Criar uma classe State concreta para cada estado
		poss�vel do objeto. As classes criadas nesta sess�o devem implementar
		a interface criada no passo 1 e adicionar os m�todos n�o
		implementados.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">

public class AlunoEstadoAprovado implements AlunoEstado {

	@Override
	public AlunoEstado aprovado() {

	}

	@Override
	public AlunoEstado reprovado() {

	}

	@Override
	public AlunoEstado provaFinal() {

	}

}

		</pre>
	</div>


	<br>
	<p>Passo 3 - Implementar os m�todos na classe State de modo que a
		classe direcione para um proximo estado possivel, ou a si mesma. Como
		exemplo apresentado.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
		

public class AlunoEstadoAprovado implements AlunoEstado {

	@Override
	public AlunoEstado aprovado() {
		return this;
	}

	@Override
	public AlunoEstado reprovado() {
		return new AlunoEstadoReprovado();
	}

	@Override
	public AlunoEstado provaFinal() {
		return new AlunoEstadoProvaFinal();
	}

}
		
		</pre>
	</div>

	<br>
	<p>Passo 4 - Repetir os passos 2 e 3 para as demais vari�veis de
		estado exeto para "reprovado" pois neste caso representa o estado
		final do objeto, n�o podendo direcionar para nenhum outro estado. A
		classe AlunoEstadoReprovado deve ficar como exemplo.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
		
public class AlunoEstadoReprovado implements AlunoEstado {

	@Override
	public AlunoEstado aprovado() {
		return null;
	}

	@Override
	public AlunoEstado reprovado() {
		return null;
	}

	@Override
	public AlunoEstado provaFinal() {
		return null;
	}

}

		
		</pre>
	</div>


	<br>
	<p>Passo 5 - Assinar o m�todo acaoAluno na interface para que as
		demais classes o implementem de acordo com o estado. A interface deve
		ficar do seguinte modo.</p>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public interface AlunoEstado {

	AlunoEstado aprovado();
	
	AlunoEstado reprovado();
	
	AlunoEstado provaFinal();
	
	EstadoAluno acaoAluno();
}

		</pre>
	</div>

	<br>
	<p>Passo 6 - Implemente as a��es em todas as classes de estado e
		remova o m�todo acaoAluno da classe cliente. A classe
		AlunoEstadoAprovado deve ficar da seguinte maneira</p>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">

public class AlunoEstadoAprovado implements AlunoEstado {

	...

	@Override
	public String acaoAluno() {
		return EstadoAluno.Aprovado;
	}

}
		</pre>
	</div>


	<br>
	<p>Passo 7 - Substituir o atributo que controla o estado na classe
		cliente por um atributo do tipo AlunoEstado. Conforme Exemplo abaixo.</p>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">

public class AlunoGraduacao {

...
	private AlunoEstado estadoAluno;
	
	public AlunoGraduacao(String nome, String matricula) {
		this.nome = nome;
		this.matricula = matricula;
	}
...
}
	
		</pre>
	</div>
	
	<p>Passo 8 - Realizar altera��es para todas as classes que representam estado
	e depois validar a saida com teste. Ajustando apenas para buscar a ac�o dentro do atributo do tipo
	AlunoEstado. Segue exemplo do teste.</p>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public class TestarAluno {

	@Test
	public void testarAlunoAprovado(){
		AlunoGraduacao alunoGraduacao = 
				new AlunoGraduacao("Marcelo Moreno", "123456");
		List&lt;Double&gt; notas = new ArrayList&lt;Double&gt;();
		
		notas.add(7.0);
		notas.add(9.0);
		notas.add(8.0);
		notas.add(9.0);
		
		alunoGraduacao.setFaltas(10);		
		alunoGraduacao.setNotas(notas);
		alunoGraduacao.calcularAprovacaoAluno();
		
		Assert.assertTrue(EstadoAluno.Aprovado.equals(
				alunoGraduacao.getEstadoAluno().acaoAluno()));
	}
}
	
		</pre>
	</div>
	



	<!-- Button trigger modal -->
	<button type="button" class="btn btn-primary btn-lg"
		data-toggle="modal" data-target="#myModal"
		style="position: fixed; bottom: 50px; right: 50px;">Exibir
		Problema</button>

	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<mc:condicionaisMudamEstado/>
			<div class="modal-content">
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="prettify/prettify_run.js"></script>