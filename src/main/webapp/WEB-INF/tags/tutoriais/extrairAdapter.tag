<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@taglib tagdir="/WEB-INF/tags/mauscheiros" prefix="mc"%>
<div style="margin-left: 25px;">
	<h2>Tutorial Refatora��o</h2>

	<h3>Extrair Adapter</h3>

	<p>Caso queria acompanhar esse tutorial usando o mesmo contexo
		basta clicar no link, fazer o download do projeto e buscar a
		refatora��o deste tutorial.</p>

	<a
		href="https://luizfernando_andrade@bitbucket.org/luizfernando_andrade/torefactor.git">
		projeto_para_download</a> <br> <br> <br>

	<p>Passo 1 - Criar uma nova classe para cada implementa��o de
		vers�o diferente, no caso apresentado ser� uma classe para ipv4 e
		outra para ipv6.</p>
	<br>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
//classes necess�rias para adapta��o da 
//conex�o de acordo com o tipo de ip

public class AccessIPV4 {

}

public class AccessIPV6 {

}

		</pre>
	</div>
	<br>
	<p>Passo 2 - Mover o atributo referente � classe IPV4 e seus
		respectivos m�todos para a classe AccessIPV4;</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">

public class AccessIPV4 {

	IPV4 ipv4 = new IPV4();
	
	public void criarIP(String ip, String mask){
		ipv4.setEndereco(ip);
		ipv4.setMascara(mask);
	}
	public String direcionarIP(){
		return ipv4.direcionarIP(ipv4);
	}
}
		</pre>
	</div>


	<br>
	<p>Passo 3 - Repetir o passo 2 para o atributo referente a classe
		IPV6.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
		
public class AccessIPV6 {

	IPV6 ipv6 = new IPV6();

	public void criarIPV6(String ip, String mask){
		ipv6.setEndereco(ip);
		ipv6.setMascara(mask);
	}
	public String direcionarIP(){
		return ipv6.direcionarIPV6(ipv6);
	}
}	
		</pre>

	</div>
	<br>
	<p>Passo 4 - Tornar a classe AccessPoint Abstrata e manter a
		assinatura dos m�todos.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
		
public abstract class AccessPoint {

	public abstract void criarIP(String ip, String mask);
	
	public abstract String direcionarIP();
}

		</pre>
	</div>


<br>
<p>Passo 5 - Extender a classe AccesPoint na nova classe AccesIPV6 e
	adaptar os m�todos. Na classe AccessIPV4 basta extender a classe
	AccesPoint pois os m�tdos possuem a mesma asinatura da classe pai.</p>

<div style="width: 720px;">
	<?prettify?>
	<pre class="prettyprint">
		
public class AccessIPV6 extends AccessPoint{

	IPV6 ipv6 = new IPV6();

	public void criarIPV6(String ip, String mask){
		ipv6.setEndereco(ip);
		ipv6.setMascara(mask);
	}
	public String direcionarIPV6(){
		return ipv6.direcionarIPV6(ipv6);
	}
	@Override
	public void criarIP(String ip, String mask) {
		criarIPV6(ip, mask);
	}
	@Override
	public String direcionarIP() {
		return direcionarIPV6();
	}
}


		</pre>
</div>

<br>
<p>Passo 6 - Ajustar a classe de teste para usar a nova estrutura e
	validar a sa�da, que deve continuar inalterada.</p>

<div style="width: 720px;">
	<?prettify?>
	<pre class="prettyprint">
	
public class TestarAccessIPV4 {

	@Test
	public void TestarDirecionamentoIPV4(){
		AccessPoint acesso = new AccessIPV4();
		acesso.criarIP("192.168.4.4", "255.255.255.0");
		
		Assert.assertTrue(acesso.direcionarIP().contains("IPV4"));
	}
}
		</pre>
</div>


<!-- Button trigger modal -->
<button type="button" class="btn btn-primary btn-lg" data-toggle="modal"
	data-target="#myModal"
	style="position: fixed; bottom: 50px; right: 50px;">Exibir
	Problema</button>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<mc:codigoDirecionadoVersoes/>
		<div class="modal-content">
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
			</div>
		</div>
	</div>
</div>
</div>
<script src="prettify/prettify_run.js"></script>