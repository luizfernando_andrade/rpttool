<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@taglib tagdir="/WEB-INF/tags/mauscheiros" prefix="mc"%>
<div style="margin-left: 25px;">
	<h2>Tutorial Refatora��o</h2>

	<h3>Substituir distin��o um/muitos por Composite</h3>

	<p>Caso queria acompanhar esse tutorial usando o mesmo contexo
		basta clicar no link, fazer o download do projeto e buscar a
		refatora��o deste tutorial.</p>

	<a
		href="https://luizfernando_andrade@bitbucket.org/luizfernando_andrade/torefactor.git">
		projeto_para_download</a> <br> <br> <br>

	<p>Passo 1 - Ap�s identificar os m�todos de manipula��o duplicada
		criar uma nova classe(Composite) que receba no construtor a lista a
		ser manipulada e criar o atributo referente a lista.</p>
	<br>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
package refatoracao.pedido.pojo;
		
public class ManipularMuitosPedidosComposite {


	private List&lt;Pedido&gt; pedidos;

	public ManipularMuitosPedidosComposite(List&lt;Pedido&gt; pedidos) {
		this.pedidos = pedidos;
	}

}

		</pre>
	</div>
	<br>
	<p>Passo 2 - Aplicar a refatoracao extrair m�todo onde a lista �
		manipulada e torn�-lo p�blico.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public class ManipularPedido {

	public List&lt;Pedido&gt; liberarPedido(List&lt;Pedido&gt; pedidos){
		return liberar(pedidos);
	}

	public List&lt;Pedido&gt; liberar(List&lt;Pedido&gt; pedidos) {
		for (Pedido pedido : pedidos) {
			pedido.setSituacaoPedido(SituacaoPedido.Liberado);
		}
		return pedidos;
	}
	...
}

		</pre>
	</div>


	<br>
	<p>Passo 3 - Mover o novo m�todo para a classe Composite rec�m
		criada e remover o par�metro do m�todo</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public class ManipularMuitosPedidosComposite implements CompositePedido {

	private List&lt;Pedido&gt; pedidos;

	public ManipularMuitosPedidosComposite(List&lt;Pedido&gt; pedidos) {
		this.pedidos = pedidos;
	}
	
	public List&lt;Pedido&gt; liberar() {
		for (Pedido pedido : pedidos) {
			pedido.setSituacaoPedido(SituacaoPedido.Liberado);
		}
		return pedidos;
	}
}

		
		</pre>
	</div>

	<br>
	<p>Passo 4 - Aplicar refatoracao extrair interface na classe
		composite rec�m criada e assinar o m�todo liberar().</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
		
public interface CompositePedido {

	public List&lt;Pedido&gt; liberar();
}
		
		</pre>
	</div>


	<br>
	<p>Passo 5 - Criar um nova classe para a manipula��o do objeto
		�nico que implemente a mesma instancia da classe de manipula��o de
		muitos e receba como par�metro no construtor o objeto a ser
		manipulado.</p>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public class ManipularUnicoPedidoComposite implements CompositePedido{

	private Pedido pedido;

	public ManipularUnicoPedidoComposite(Pedido pedido) {
		this.pedido = pedido;
	}

}

		</pre>
	</div>

	<br>
	<p>Passo 6 - Aplicar os passos de 2 a 3 para a nova classe.</p>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">

public class ManipularUnicoPedidoComposite implements CompositePedido{

	private Pedido pedido;

	public ManipularUnicoPedidoComposite(Pedido pedido) {
		this.pedido = pedido;
	}
	
	public List&lt;Pedido&gt; liberar() {
		List&lt;Pedido&gt; pedidos = new ArrayList&lt;Pedido&gt;();
		pedido.setSituacaoPedido(SituacaoPedido.Liberado);
		pedidos.add(pedido);
		return pedidos;
	}
}
		</pre>
	</div>


	<br>
	<p>Passo 7 - No construtor da classe cliente informar qual
		instancia do composite ir� manipular os objetos e criar o atributo na
		classe.</p>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">

public class ManipularPedido {

	private CompositePedido composite;

	public ManipularPedido(CompositePedido composite) {
		this.composite = composite;
	}
	...
}
	
		</pre>
	</div>
	<br>
	<p>Passo 8 - Remova o par�metro do m�todo liberarPedido, ajuste
		para usar a inst�ncia do objeto composite e tamb�m remova o m�todo
		duplicado.</p>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public class ManipularPedido {

	private CompositePedido composite;

	public ManipularPedido(CompositePedido composite) {
		this.composite = composite;
	}
	
	public List&lt;Pedido&gt; liberarPedido(){
		return composite.liberar();
	}

}

		</pre>
	</div>

	<br>

	<p>Passo 9 - Ajustar clientes para usar a nova estrutura e validar
		as altera��es com a execu��o do teste.</p>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public class TestarLiberacaoPedido {

	@Test
	public void liberarPedidoUnico(){
		boolean pedidoPendente = false;
		
		Pedido pedido = new Pedido(SituacaoPedido.Pendente);
		pedido.setDataPedido(Calendar.getInstance());
		
		List&lt;Item&gt; itens = new ArrayList&lt;Item&gt;();
		itens.add(new Item("protetor auricular"));
		
		pedido.setItens(itens);
		ManipularUnicoPedidoComposite unicoPedidoManipulador = 
				new ManipularUnicoPedidoComposite(pedido);
		ManipularPedido manipularPedido = 
		new ManipularPedido(unicoPedidoManipulador);
		
		for (Pedido unidade : manipularPedido.liberarPedido()) {
			if (unidade.getSituacaoPedido().equals(SituacaoPedido.Pendente)){
				pedidoPendente = true;
			}
		} 
		
		Assert.assertTrue(!pedidoPendente);
	}
	
	@Test
	public void liberarPedidos(){
		boolean pedidoPendente = false;
		List&lt;Item&gt; itens = new ArrayList&lt;Item&gt;();
		itens.add(new Item("protetor auricular"));
		itens.add(new Item("protetor bucal"));
		itens.add(new Item("protetor nasal"));
		
		List&lt;Pedido&gt; pedidos = new ArrayList&lt;&gt;();
		
		Pedido pedido = new Pedido(SituacaoPedido.Pendente);
		pedido.setDataPedido(Calendar.getInstance());
		pedido.setItens(itens);
		pedidos.add(pedido);
		Pedido pedido2 = new Pedido(SituacaoPedido.Pendente);
		pedido.setDataPedido(Calendar.getInstance());
		pedido.setItens(itens);
		pedidos.add(pedido2);
		Pedido pedido3 = new Pedido(SituacaoPedido.Pendente);
		pedido.setDataPedido(Calendar.getInstance());
		pedido.setItens(itens);
		pedidos.add(pedido3);
		
		ManipularMuitosPedidosComposite muitosPedidosManipulador = 
				new ManipularMuitosPedidosComposite(pedidos);
		ManipularPedido manipularPedido = 
		new ManipularPedido(muitosPedidosManipulador);
		
		for (Pedido unidade : manipularPedido.liberarPedido()) {
			if (unidade.getSituacaoPedido().equals(SituacaoPedido.Pendente)){
				pedidoPendente = true;
			}
		} 
		Assert.assertTrue(!pedidoPendente);
	}
	
}
		</pre>
	</div>




	<!-- Button trigger modal -->
	<button type="button" class="btn btn-primary btn-lg"
		data-toggle="modal" data-target="#myModal"
		style="position: fixed; bottom: 50px; right: 50px;">Exibir
		Problema</button>

	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<mc:manipulacaoDuplicadaObjetos/>
			<div class="modal-content">
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="prettify/prettify_run.js"></script>