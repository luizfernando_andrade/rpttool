<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@taglib tagdir="/WEB-INF/tags/mauscheiros" prefix="mc"%>
<div style="margin-left: 25px;">
	<h2>Tutorial Refatora��o</h2>

	<h3>Encadear Construtores</h3>

	<p>Caso queria acompanhar esse tutorial usando o mesmo contexo
		basta clicar no link, fazer o download do projeto e buscar a
		refatora��o deste tutorial.</p>

	<a
		href="https://luizfernando_andrade@bitbucket.org/luizfernando_andrade/torefactor.git">
		projeto_para_download</a> <br>
	<br>
	<br>

	<p>Passo 1 - Identificar o construtor mais completo para que esse
		possa ser usado no encadeamento. O mais completo � aquele que possui
		todos os atributos da classe em sua assinaturo ou o maior n�mero
		deles.</p>
	<br>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public class Funcionario {
	
	private String nome;
	private String cpf;
	private Date nascimento;
	private List&lt;Dependente&gt; dependentes;
	private Boolean possuiCNH;
	
	public Funcionario( String nome, String cpf, Date nascimento, 
			List&lt;Dependente&gt; dependentes, Boolean possuiCNH) {
		this.nome = nome;
		this.cpf = cpf;
		this.nascimento = nascimento;
		this.dependentes = dependentes;
		this.possuiCNH = possuiCNH;
	}
	...
}

		</pre>
	</div>
	<br>
	<p>Passo 2 - Substituir as atribui��es feitas nos demais contrutores
		por chamadas ao construtor mais completo assim reduzir� a duplica��o
		de c�digo na classe. Os atributos que n�o existirem nos construtores
		menos completos, devem ser atribu�dos null</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">

public class Funcionario {
	
	private String nome;
	private String cpf;
	private Date nascimento;
	private List&lt;Dependente&gt; dependentes;
	private Boolean possuiCNH;
	
	public Funcionario( String nome, String cpf, Date nascimento, 
			List&lt;Dependente&gt; dependentes, Boolean possuiCNH) {
		this.nome = nome;
		this.cpf = cpf;
		this.nascimento = nascimento;
		this.dependentes = dependentes;
		this.possuiCNH = possuiCNH;
	}
	public Funcionario( String nome, String cpf, Date nascimento, 
			List&lt;Dependente&gt; dependentes) {
		this(nome, cpf, nascimento, dependentes, null);
	}
	public Funcionario( String nome, String cpf, Date nascimento) {
		this(nome, cpf, nascimento, null, null);
	}
	public Funcionario( String nome, String cpf, Date nascimento, 
			boolean possuiCNH) {
		this(nome, cpf, nascimento, null, possuiCNH);
	}
}
		</pre>
	</div>


	<br>
	<p>Passo 3 - Compilar e testar para certificar-se de que tudo
		continua como antes.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
		
public class TestarFuncionario {
	
	@Test
	public void testarConstrutorCompleto(){
		Calendar nascimento = new GregorianCalendar(1988, 10, 10);
		List&lt;Dependente&gt; dependentes = new ArrayList&lt;&gt;();
		dependentes.add(new Dependente("Roberta"));
		Funcionario funcionarioDadosCompletos = 
			new Funcionario("Ronaldo", "12345678909",nascimento.getTime(),
					dependentes, Boolean.TRUE);
		
		Assert.assertTrue(funcionarioDadosCompletos.getNome()!= null
			&& funcionarioDadosCompletos.getCpf() != null
			&& funcionarioDadosCompletos.getNascimento() != null
			&& funcionarioDadosCompletos.getDependentes().size() > 0
			&& funcionarioDadosCompletos.isPossuiCNH());
	}
	@Test
	public void testarConstrutorSemCNHSemDependentes(){
		Calendar nascimento = new GregorianCalendar(1988, 10, 10);
		Funcionario funcionarioDadosCompletos = 
			new Funcionario("Ronaldo", "12345678909",nascimento.getTime());
		
		Assert.assertTrue(funcionarioDadosCompletos.getNome()!= null
			   && funcionarioDadosCompletos.getCpf() != null
		       && funcionarioDadosCompletos.getNascimento() != null);
	}

}
	
		
		</pre>
	</div>


	<!-- Button trigger modal -->
	<button type="button" class="btn btn-primary btn-lg"
		data-toggle="modal" data-target="#myModal"
		style="position: fixed; bottom: 50px; right: 50px;">Exibir
		Problema</button>

	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<mc:construtorescomcodigoduplicado/>
			<div class="modal-content">
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="prettify/prettify_run.js"></script>