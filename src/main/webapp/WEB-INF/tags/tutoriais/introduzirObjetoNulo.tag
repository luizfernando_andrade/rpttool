<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@taglib tagdir="/WEB-INF/tags/mauscheiros" prefix="mc"%>
<div style="margin-left: 25px;">
	<h2>Tutorial Refatora��o</h2>

	<h3>Introduzir Objeto Nulo</h3>

	<p>Caso queria acompanhar esse tutorial usando o mesmo contexo
		basta clicar no link, fazer o download do projeto e buscar a
		refatora��o deste tutorial.</p>

	<a
		href="https://luizfernando_andrade@bitbucket.org/luizfernando_andrade/torefactor.git">
		projeto_para_download</a> <br> <br> <br>

	<p>Passo 1 - Criar uma classe abaixo na hierarquia do objeto que �
		tratado com verifica��o de objeto nulo.</p>
	<br>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
package refatoracao.nulo.pojo;

public class UsuarioNulo {

}

		</pre>
	</div>
	<br>
	<p>Passo 2 - Atribuir no construtor da classe filha uma chamada
		para a classe pai passando o parametro null</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">

public class UsuarioNulo extends Usuario {
	public UsuarioNulo() {
		super(null);
	}
	
}
		</pre>
	</div>


	<br>
	<p>Passo 3 - Implementar o mesmo m�todo que tem o condicional de
		verifica��o na classe filha para evitar o erro de NullPointerException
		pois ir� chamar o m�todo da classe de objeto nulo via polimorfismo.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
		
public class UsuarioNulo extends Usuario {
	public UsuarioNulo() {
		super(null);
	}
	
	public boolean logar(String usuario, String senha){
		return true;
	}
}
	
		</pre>
	</div>


	<!-- Button trigger modal -->
	<button type="button" class="btn btn-primary btn-lg"
		data-toggle="modal" data-target="#myModal"
		style="position: fixed; bottom: 50px; right: 50px;">Exibir
		Problema</button>

	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<mc:tratamentoDeObjetoNulo/>
			<div class="modal-content">
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="prettify/prettify_run.js"></script>