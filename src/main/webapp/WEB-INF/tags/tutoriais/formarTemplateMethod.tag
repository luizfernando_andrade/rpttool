<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@taglib tagdir="/WEB-INF/tags/mauscheiros" prefix="mc"%>
<div style="margin-left: 25px;">
	<h2>Tutorial Refatora��o</h2>
	
	<h3>Formar Template Method</h3>
	
	<p>Caso queria acompanhar esse tutorial usando o mesmo contexo basta clicar no link, fazer o download do 
		projeto e buscar a refatora��o deste tutorial. </p>
	
	<a href="https://luizfernando_andrade@bitbucket.org/luizfernando_andrade/torefactor.git">
	projeto_para_download</a>
	<br><br><br>

	<p>Passo 1 - Identificar os m�todos similares nas classes de mesma hierarquia.
	No caso apresetando eles est�o nas classes CursoIntensivo e CursoExtensivo. 
	Primeiro ser� tratada a classe CursoIntensivo. Ela possui m�todos com a 
	mesma assinatura dos m�todos ca classe CursoExtensivo por�m com caracter�sticas
	diferente.</p>
	<br>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
package maucheiro.curso.pojo;

public class CursoIntensivo extends Curso{

	@Override
	public String infoCurso() {
		return "Curso pr� vestibular intensivo, 6 meses de dura��o";
	}
	@Override
	public String prepararAula() {
		return "Conte�do sintetizado para o vestibular";
	}
	@Override
	public String avaliacaoSemanal() {
		return "Conte�do acumulativopara " + tipoCurso();
	}
	@Override
	public String tipoCurso() {
		return "Curso Intensivo";
	}
}

		</pre>
	</div>
	<br>
	<p>Passo 2 - Identificar os m�todos que necessitam ser executados
		juntos e compor um novo m�todo nas classes seguindo a sequencia de
		execu��o.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
		
package maucheiro.curso.pojo;

public class CursoIntensivo extends Curso{
...
	public String descricaoCurso(){
		
		return infoCurso()
		.concat(prepararAula())
		.concat(avaliacaoSemanal());
	}
...
}
		</pre>
	</div>
	
	
	<br>
	<p>Passo 3 - Aplicar esta refatora��o nas demais classes da hierarquia,
		que possuem m�todos similares, mantendo a mesma assinatura nos m�todos
		novos. Como o novo m�todo faz chamada aos m�todos j� existentes na hierarquia
		as classes ficaram com o mesmo m�todo.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">

public class CursoExtensivo extends Curso {
...
	public String descricaoCurso(){
		
		return infoCurso()
		.concat(prepararAula())
		.concat(avaliacaoSemanal());
	}
...

}

		</pre>
	</div>
	
	
	<br>

	<p>Passo 4 - Mova o m�todo similar nas classes para a classe pai,
		aplicando a refatora��o subir m�todo na hierarquia. Desse jeito 
		as classes ficam livre de duplica��o de c�digo, mantendo o m�todo 
		descricaoCurso apenas na classe pai da hierarquia e apagando das 
		classes filhas.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
		
package refatoracao.curso.pojo;

public abstract class Curso {
	
	public abstract String infoCurso();
	
	public abstract String prepararAula();
	
	public abstract String avaliacaoSemanal();
	
	public abstract String tipoCurso();

	public String descricaoCurso() {
		
		return infoCurso()
		.concat(prepararAula())
		.concat(avaliacaoSemanal());
	}
	
}

	
		</pre>
	</div>
	
	<br>
	<p>Passo 5 - Realizar a chamada do novo m�todo na classe de
		contexto e validar se a sa�da continua a mesma.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
		
public class TesteCursoPreVestibular {

	Curso curso = null;
	
	@Test
	public void testarCursoIntensivo(){
		curso = new CursoIntensivo();
		String descricaoCurso = curso.descricaoCurso();
		
		Assert.assertTrue(descricaoCurso.
				contains("Intensivo"));
	}
	
	@Test
	public void testarCursoExtensivo(){
		curso = new CursoExtensivo();
		String descricaoCurso = curso.descricaoCurso();
		
		Assert.assertTrue(descricaoCurso.
				contains("Extensivo"));
	}
}

		</pre>
	</div>	
	
	
	<!-- Button trigger modal -->
<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal"
style="position:fixed;bottom: 50px;right: 50px;">
  Exibir Problema
</button>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
      <mc:metodossemelhantesemhierarquia/>
    <div class="modal-content">
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>
</div>
<script src="prettify/prettify_run.js"></script>