<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@taglib tagdir="/WEB-INF/tags/mauscheiros" prefix="mc"%>
<div style="margin-left: 25px;">
	<h2>Tutorial Refatora��o</h2>

	<h3>Unificar Interfaces com Adapter</h3>

	<p>Caso queria acompanhar esse tutorial usando o mesmo contexo
		basta clicar no link, fazer o download do projeto e buscar a
		refatora��o deste tutorial.</p>

	<a
		href="https://luizfernando_andrade@bitbucket.org/luizfernando_andrade/torefactor.git">
		projeto_para_download</a> <br> <br> <br>

	<p>Passo 1 - Extrair interface da classe de maior preferencia do
		cliente, no exemplo em quest�o a classe Atributo.</p>
	<br>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">

package refatoracao.arquivointerface.pojo;

public interface CampoArquivo {

}

		</pre>
	</div>
	<br>
	<p>Passo 2 - Assinar os m�todos que s�o comuns entre a classe
		Atributo e a classe Celula na nova interface.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public interface CampoArquivo {

	List&lt;String&gt; getConteudo();

	void adicionarConteudo(String conteudo);

}
		</pre>
	</div>


	<br>
	<p>Passo 3 - Implementar a classe para que seja filha da nova
		interface e tenha os mesmos m�todos assinados.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public class Atributo implements CampoArquivo {
...

	@Override
	public List&lt;String&gt; getConteudo() {
		return conteudo;
	}

	@Override
	public void adicionarConteudo(String conteudo) {
		this.conteudo.add(conteudo);
	}
}
		
		</pre>
	</div>

	<br>
	<p>Passo 4 - Criar uma nova classe para auxiliar na adapta��o da
		classe Celula e nomear a nova classe para CelulaAdapter.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
		
package refatoracao.arquivointerface.pojo;

public class CelulaAdapter {

}

		
		</pre>
	</div>


	<br>
	<p>Passo 5 - Fazer com que a nova classe implemente a interface
		criada no passo um e que contenha os m�todos por ela assinado.</p>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public class CelulaAdapter implements CampoArquivo{

	@Override
	public List&lt;String&gt; getConteudo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void adicionarConteudo(String conteudo) {
		// TODO Auto-generated method stub
		
	}

}

		</pre>
	</div>

	<br>
	<p>Passo 6 - Criar uma instancia da classe a ser adaptada, no caso
		a classe Celula, na nova classe.</p>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">

public class CelulaAdapter implements CampoArquivo{

	private Celula celula = new Celula();
	
...

}
		</pre>
	</div>


	<br>
	<p>Passo 7 - Implementar os m�todos da classe CelulaAdapter
		invocando os metodos da classe Celula.</p>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
		
public class CelulaAdapter implements CampoArquivo{

	private Celula celula = new Celula();
	
	@Override
	public List&lt;String&gt; getConteudo() {
		return celula.getValores();
	}

	@Override
	public void adicionarConteudo(String conteudo) {
		celula.setValores(conteudo);
	}
}

		</pre>
	</div>
	<br>
	<p>Passo 8 - Executar o teste e validar a sa�da para certificar que
		a altera��o na gerou impacto com a cria��o das novas classes.</p>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">

public class TestarArquivoCSV {

	@Test
	public void montarArquivoCSV(){
		ArquivoCSV arquivo = new ArquivoCSV();
		
		arquivo.addAtributo("teste");
		arquivo.addAtributo("Teste2");
		arquivo.addAtributo("Teste3");
		arquivo.addAtributo("Teste4");
		arquivo.getAtributo().setAtributosLinha(2);
		arquivo.montarArquivo();
		
		Assert.assertTrue(arquivo.getAtributo() instanceof Atributo);
		
	}
}
		</pre>
	</div>

	<!-- Button trigger modal -->
	<button type="button" class="btn btn-primary btn-lg"
		data-toggle="modal" data-target="#myModal"
		style="position: fixed; bottom: 50px; right: 50px;">Exibir
		Problema</button>

	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<mc:classesComInterfacePreferida/>
			<div class="modal-content">
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="prettify/prettify_run.js"></script>