<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@taglib tagdir="/WEB-INF/tags/mauscheiros" prefix="mc"%>
<div style="margin-left: 25px;">
	<h2>Tutorial Refatora��o</h2>

	<h3>Encapsular Composite Com Builder</h3>

	<p>Caso queria acompanhar esse tutorial usando o mesmo contexo
		basta clicar no link, fazer o download do projeto e buscar a
		refatora��o deste tutorial.</p>

	<a
		href="https://luizfernando_andrade@bitbucket.org/luizfernando_andrade/torefactor.git">
		projeto_para_download</a> <br> <br> <br>

	<p>Passo 1 - Ap�s identificar o composite com a cria��o de objetos,
		criar uma classe builder com nome sugestivo.</p>
	<br>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
		
package refatoracao.conexao.pojo;

public class ConexaoBuilder {

}

		</pre>
	</div>
	<br>
	<p>passo 2 - Crie um m�todo no builder capaz de construir objetos
		do tipo final no composite. Neste caso objetos do tipo Conexao.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
		
public class ConexaoBuilder {

	public Conexao montarConexao(){
		
	}
}

		</pre>
	</div>


	<br>
	<p>Passo 3 - Caso existam par�metros necess�rios para cria��o de
		objetos complementares fa�a com que o builder os assine no m�todo de
		cria��o.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public class ConexaoBuilder {

	public Conexao montarConexao(String usuario, 
			String senha, TipoBaseDados baseDados){
		
	}
}
			
		</pre>
	</div>

	<br>
	<p>Passo 4 - Mova a l�gica do composite para o novo m�todo no Builder.
	Deste modo reduz o acoplamento do cliente com as classes do composite.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">

public class ConexaoBuilder {

	public Conexao montarConexao(String usuario, 
			String senha, TipoBaseDados baseDados){
		
		ConfiguraConexao configuraConexao = 
				new ConfiguraConexao(usuario,senha,baseDados);
		
		TipoConexao tipoConexao = new TipoConexao(configuraConexao);
		
		Conector conector = new Conector(tipoConexao);
		
		Conexao conexao = conector.criarConexao();
		return conexao;
	}
}
		</pre>
	</div>

	<br>
	<p>Passo 5 - Modifique a classe cliente para que construa o objeto
		a partir do builder e valide com o teste.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">

public class TestConexao {
	
	@Test
	public void testarConexao(){
		ConexaoBuilder builder = new ConexaoBuilder();
		
		Conexao conexao = builder.montarConexao("admin", "abcd01", TipoBaseDados.PRODUCAO);
		
		Assert.assertTrue(conexao != null);
	}
}
		</pre>
	</div>

	<!-- Button trigger modal -->
	<button type="button" class="btn btn-primary btn-lg"
		data-toggle="modal" data-target="#myModal"
		style="position: fixed; bottom: 50px; right: 50px;">Exibir
		Problema</button>

	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<mc:criacaoComplexadeObjetos/>
			<div class="modal-content">
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="prettify/prettify_run.js"></script>