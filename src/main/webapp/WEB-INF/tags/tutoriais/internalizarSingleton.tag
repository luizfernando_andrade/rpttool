<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@taglib tagdir="/WEB-INF/tags/mauscheiros" prefix="mc"%>
<div style="margin-left: 25px;">
	<h2>Tutorial Refatora��o</h2>

	<h3>Internalizar Singleton</h3>

	<p>Caso queria acompanhar esse tutorial usando o mesmo contexo
		basta clicar no link, fazer o download do projeto e buscar a
		refatora��o deste tutorial.</p>

	<a
		href="https://luizfernando_andrade@bitbucket.org/luizfernando_andrade/torefactor.git">
		projeto_para_download</a> <br> <br> <br>

	<p>Passo 1 - Identificar os m�todos p�blicos n�o est�ticos da
		classe singleton.</p>
	<br>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
		
public class CalculosEquacaoSegundoGrau {

...
	public Double calcularDelta(double a, double b, double c){
		return (b*b)- 4*a*c;
	}
	public Double calcularX1(double a, double b, double c, double delta){
		return (-b+Math.sqrt(delta))/2*a;
	}
	public Double calcularX2(double a, double b, double c, double delta){
		return (-b-Math.sqrt(delta))/2*a;
	}
}

		</pre>
	</div>
	<br>
	<p>Passo 2 - Mover os m�todos publicos que n�o manipulam a
		instancia do singleton para a classe que o usa. 
		Caso necess�rio criar vari�veis que antes eram par�metro de m�todo,
		neste caso a vari�vel delta foi criada.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public class EquacaoSegundoGrau {

	private double delta;
...
	
	public Double valorDelta(){
		return CalculosEquacaoSegundoGrau.
				getInstancia().calcularDelta(a, b, c);
	}
	
	public Double calcularDelta(double a, double b, double c){
		return (b*b)- 4*a*c;
	}
	public Double calcularX1(double a, double b, double c, double delta){
		return (-b+Math.sqrt(delta))/2*a;
	}
	public Double calcularX2(double a, double b, double c, double delta){
		return (-b-Math.sqrt(delta))/2*a;
	}
	
}

		</pre>
	</div>


	<br>
	<p>Passo 3 - Remover m�todo intermediario que realiza a chamada ao
		Singleton.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public class EquacaoSegundoGrau {

	private double delta;
...
	
	public Double calcularDelta(double a, double b, double c){
		return (b*b)- 4*a*c;
	}
	public Double calcularX1(double a, double b, double c, double delta){
		return (-b+Math.sqrt(delta))/2*a;
	}
	public Double calcularX2(double a, double b, double c, double delta){
		return (-b-Math.sqrt(delta))/2*a;
	}
	
}
			
		</pre>
	</div>

	<br>
	<p>Passo 4 - Ajustar m�todos do singleton para que usem atributos
		da classe, que antes eram passados via par�metro.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public class EquacaoSegundoGrau {

	private double delta;
...
	
	public Double calcularDelta(){
		return (b*b)- 4*a*c;
	}
	public Double calcularX1(){
		return (-b+Math.sqrt(delta))/2*a;
	}
	public Double calcularX2(){
		return (-b-Math.sqrt(delta))/2*a;
	}
	
}
		</pre>
	</div>

	<br>
	<p>Passo 5 - Ajustar chamada do cliente para usar o novo m�todo e
		validar a saida com o teste. Ap�s a mudan�a remova a classe Singleton.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">

public class TestarEquacaoSegundoGrau {

	@Test
	public void realizarCalculoDelta(){
		
		EquacaoSegundoGrau equacao = new EquacaoSegundoGrau();
		equacao.setA(3);
		equacao.setB(-7);
		equacao.setC(2);
		
		Double delta = equacao.calcularDelta();
		
		Assert.assertTrue(delta == 25.0);
		
	}
}
		</pre>
	</div>

	<!-- Button trigger modal -->
	<button type="button" class="btn btn-primary btn-lg"
		data-toggle="modal" data-target="#myModal"
		style="position: fixed; bottom: 50px; right: 50px;">Exibir
		Problema</button>

	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<mc:singletonDesnecessario/>
			<div class="modal-content">
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="prettify/prettify_run.js"></script>