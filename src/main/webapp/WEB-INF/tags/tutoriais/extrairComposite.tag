<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@taglib tagdir="/WEB-INF/tags/mauscheiros" prefix="mc"%>
<div style="margin-left: 25px;">
	<h2>Tutorial Refatora��o</h2>

	<h3>Extrair Composite</h3>

	<p>Caso queria acompanhar esse tutorial usando o mesmo contexo
		basta clicar no link, fazer o download do projeto e buscar a
		refatora��o deste tutorial.</p>

	<a
		href="https://luizfernando_andrade@bitbucket.org/luizfernando_andrade/torefactor.git">
		projeto_para_download</a> <br> <br> <br>

	<p>Passo 1 - Ap�s identificar classes composite com c�digo
		semelhante, criar uma classe que se tornar� um composite e
		representar� as classes com c�digo semelhante.</p>
	<br>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">

package refatoracao.bhaskara.pojo;

public class BhaskaraComposite {

}


		</pre>
	</div>
	<br>
	<p>Passo 2 - Nas classe BhaskaraX1 extrair o calculo da raiz de
		delta para um novo m�todo.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">

public class BhaskaraX1 {
	
	private DeltaComposite delta;
	private Double a;
	private Double b;

	public BhaskaraX1(Double a, Double b, DeltaComposite delta) {
		this.a = a;
		this.b = b;
		this.delta = delta;
	}
	
	public double calcularBhaskaraX1() {
		return (-b + raizDelta())/2*a;
	}

	private double raizDelta() {
		return Math.sqrt( delta.calcularDelta());
	}
}
		</pre>
	</div>


	<br>
	<p>Passo 3 - Aplicar passo 3 na classe BhaskaraX2.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public class BhaskaraX2 {
	
	private DeltaComposite delta;
	private Double a;
	private Double b;

	public BhaskaraX2(Double a, Double b, DeltaComposite delta) {
		this.a = a;
		this.b = b;
		this.delta = delta;
	}

	public double calcularBhaskaraX2() {
		return (-b - raizDelta())/2*a;
	}

	private double raizDelta() {
		return Math.sqrt( delta.calcularDelta());
	}
}
		</pre>
	</div>


	<br>

	<p>Passo 4 - Fazer com que as classes BhaskaraX1 e BhaskaraX2 sejam
		filhas da nova classe composite.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">

public class BhaskaraX1 extends BhaskaraComposite {

}

public class BhaskaraX2 extends BhaskaraComposite {

}


		</pre>
	</div>

	<br>
	<p>Passo 5 - Subir o m�todo de calculo da raiz de delta na
		hierarquia.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public class BhaskaraComposite {

	public double raizDelta() {
		return Math.sqrt( delta.calcularDelta());
	}
}

		</pre>
	</div>

	<br>
	<p>Passo 6 - Mover atributos para a classe pai para reduzir
		duplica��o e criar um construtor com os parametros necess�rios para o
		c�lculo. Mudar a visibilidade dos atributos para que seja poss�vel
		acess�-los nas classes filhas</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">

public class BhaskaraComposite {

	private DeltaComposite delta;
	protected Double a;
	protected Double b;

	public double raizDelta() {
		return Math.sqrt( delta.calcularDelta());
	}
}
		</pre>
	</div>

	<br>
	<p>Passo 7 - Criar construtor na classe pai com os novos atributos.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
	
public class BhaskaraComposite {

	private DeltaComposite delta;
	protected Double a;
	protected Double b;

	public BhaskaraComposite(Double a, Double b, DeltaComposite delta) {
		this.a = a;
		this.b = b;
		this.delta = delta;
	}
	
	public double raizDelta() {
		return Math.sqrt( delta.calcularDelta());
	}
}
		</pre>
	</div>


	<br>
	<p>Passo 8 - Atribuir a cria��o das classes filhas para a o
		construtor da classe pai. Abaixo � exibida a classe BhaskaraX1,
		realizar o mesmo para BhaskaraX2.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public class BhaskaraX1 extends BhaskaraComposite {
	
	...
	
	public BhaskaraX1(Double a, Double b, DeltaComposite delta) {
		super(a, b, delta);
	}
	...
}

		</pre>
	</div>


	<br>
	<p>Passo 9 - Validar o funcionamento da classe cliente com a execu��o dos testes,
		confirmando que as altera��es n�o afetaram a sa�da da classe.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
	
public class TestarFuncao {

	@Test
	public void testarBhaskaraX1(){
		Expressao bhaskara = new Expressao();
		HashMap&lt;String, Double&gt; resultado = (HashMap&lt;String, Double&gt;) 
				bhaskara.calcularBhaskara(1.0, -2.0, -3.0);
		
		Double x1 = resultado.get("x1");
		
		System.out.println("X1 = " + x1 );
		
		Assert.assertTrue(x1 == 3.0);
	}
	@Test
	public void testarBhaskaraX2(){
		Expressao bhaskara = new Expressao();
		HashMap&lt;String, Double&gt; resultado = (HashMap&lt;String, Double&gt;) 
				bhaskara.calcularBhaskara(1.0, -2.0, -3.0);
		
		Double x2 = resultado.get("x2");
		
		System.out.println("X2 = " + x2 );
		
		Assert.assertTrue(x2 == -1.0);
	}
}
		</pre>
	</div>



	<!-- Button trigger modal -->
	<button type="button" class="btn btn-primary btn-lg"
		data-toggle="modal" data-target="#myModal"
		style="position: fixed; bottom: 50px; right: 50px;">Exibir
		Problema</button>

	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<mc:compositeEspalhadoEmHierarquia/>
			<div class="modal-content">
				<!--       <div class="modal-header"> -->
				<!--         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
				<!--         <h4 class="modal-title" id="myModalLabel">Mau Cheiro</h4> -->
				<!--       </div> -->
				<!--       <div class="modal-body"> -->
				<!--       </div> -->
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="prettify/prettify_run.js"></script>