<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@taglib tagdir="/WEB-INF/tags/mauscheiros" prefix="mc"%>
<div style="margin-left: 25px;">
	<h2>Tutorial Refatora��o</h2>
	
	<h3>Substituir L�gica Condicional por Strategy</h3>
	
	<p>Caso queria acompanhar esse tutorial usando o mesmo contexo basta clicar no link, fazer o download do 
		projeto e buscar a refatora��o deste tutorial. </p>
	
	<a href="https://luizfernando_andrade@bitbucket.org/luizfernando_andrade/torefactor.git">
	projeto_para_download</a>
	<br><br><br>
	
	<p>Passo 1 - Criar classe strategy concreta, que receber� a l�gica
		principal contida no m�todo a ser refatorado, o nome da classe deve
		expressar qual sua real intens�o. A facilitar o entendimento criamos um
		pacote com o sufixo strategy conforme exibido no trecho abaixo.</p>
	<br>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
	
package refatoracao.strategy;


public class CalcularSalarioComissaoStrategy {
	
}
	
		</pre>
	</div>
	<br>
	<p>Passo 2 - Aplicar a refatora��o mover m�todo, levando o m�todo de
		c�lculo para a classe strategy. Ap�s a mudan�a classe n�o compila, esse
		problema ser� tratado posteriormente.</p>
	
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">


package refatoracao.strategy;


public class CalcularSalarioComissaoStrategy {
	
	public Double calcularSalarioComComissao(){
	
		if (TipoFuncionario.GERENTE.equals(tipoFuncionario)){
				return getSalario() + (getSalario() * 0.3) 
					+ adicionalGerente();
			} // adiciona 30% ao sal�rio + 5% ao ano  
			else if (TipoFuncionario.COORDENADOR.equals(tipoFuncionario)){
				return getSalario() + (getSalario() * 0.2);
			} // adiciona 20% ao sal�rio 
			else {
				return getSalario() + (getSalario() * 0.1);
			} // adiciona 10% ao sal�rio
		}

}

	
		</pre>
	</div>
	
	
	<br>
	<p>Passo 3 - Passar os dados da classe contexto no caso Funcion�rio
		para o Strategy via par�metro conforme abaixo e ajustar o campos quebrados.</p>
	
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">

package refatoracao.strategy;

import refatoracao.pojo.Funcionario;

public class CalcularSalarioComissaoStrategy {
	
 public Double calcularSalarioComComissao(Funcionario funcionario){
	
	if (TipoFuncionario.GERENTE.equals(tipoFuncionario)){
		  return funcionario.getSalario() + (funcionario.getSalario() * 0.3) 
							+ funcionario.adicionalGerente();
		  } // adiciona 30% ao sal�rio + 5% ao ano  
	else if (TipoFuncionario.COORDENADOR.equals(tipoFuncionario)){
		  return funcionario.getSalario() + (funcionario.getSalario() * 0.2);
		  } // adiciona 20% ao sal�rio 
	else {
		   return funcionario.getSalario() + (funcionario.getSalario() * 0.1);
		  } // adiciona 10% ao sal�rio
		}

}

	
		</pre>
	</div>
	
	
	<br>
	
	<p>Passo 4 - Criar refer�ncia na classe de contexto no caso
		Funcionario para usar o m�todo da classe
		CalcularSalarioComissaoStrategy.</p>
	
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
	
	public Double calcularSalarioComComissao() {
		
		CalcularSalarioComissaoStrategy calcular =
			 new CalcularSalarioComissaoStrategy()
		
		return calcular.calcularSalarioComComissao(this);
		
	}
	
		</pre>
	</div>
	
	<br>
	<p>Passo 5 - Executar o teste na classe de contexto e ver se tudo
		compila e roda perfeitamente, segue implementa��o do caso de 
		teste valindando o funcionamento do c�lculo sal�rio com base 
		na profiss�o.</p>
	
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">

public class TesteCalculoSalario {

	@Test
	public void test() {
		Pessoa pessoa = new Pessoa();
		Calendar nascimento = Calendar.getInstance();
		nascimento.set(1980, 10, 15);
		
		pessoa.setDocumento("123456789");
		pessoa.setNascimento(nascimento);
		pessoa.setNome("Juliana Maria");
		
		Calendar admissao = Calendar.getInstance();
		admissao.set(2010, 10, 15);
		Salario salario = new Salario();
		salario.setValorSalario(11000.00);
		
		Funcionario funcionario = new Funcionario();
		funcionario.setAdmissao(admissao);
		funcionario.setPessoa(pessoa);
		funcionario.setSalario(salario);
		funcionario.setTipoFuncionario(TipoFuncionario.COORDENADOR);
		
		Assert.assertTrue(funcionario.calcularSalarioComComissao()
			.equals(new Double(13200.00)));
	}

}
		</pre>
	</div>
	
	<br>
	<p>Passo 6 - Criar uma classe para cada condicional determinante e
		extender da classe strategy concreta. Abaixo segue exemplo para 
		cria��o da classe CalcularSalarioComissaoStrategyGerente, 
		fazer o mesmo para, CalcularSalarioComissaoStrategyCoordenador e 
		CalcularSalarioComissaoStrategyComum em arquivos diferentes.</p>
	
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
	
package refatoracao.strategy;

import refatoracao.pojo.Funcionario;

public class CalcularSalarioComissaoStrategyGerente 
		extends CalcularSalarioComissaoStrategy {


}


		</pre>
	</div>
	
	<br>
	<p>Passo 7 - Tornar a classe Concreta strategy abstrata e criar um
		m�todo calcularComissao abstrato para que as classes filhas sejam
		obrigadas a implementa-lo. Necess�rio passar o Funcion�rio como
		par�metro para calcular os dados necess�rios.</p>
	
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
	
package refatoracao.strategy;

import refatoracao.pojo.Funcionario;

public abstract class CalcularSalarioComissaoStrategy {
	
	abstract Double calcularComissao(Funcionario funcionario);
	
}

	
		</pre>
	</div>
	
	
	<br>
	<p>Passo 8 - Caso existam m�todos adicionais para cada condi��o,
		devemos implementa-los nas classes de refer�ncia. No exemplo o m�todo
		adicionalGerente deve ser movido  da classe contexto para a classe 
		CalcularSalarioComissaoStrategy	CalcularSalarioComissaoStrategyGerente.</p>
	
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
	
package refatoracao.strategy;

import refatoracao.pojo.Funcionario;

public class CalcularSalarioComissaoStrategyGerente 
				extends CalcularSalarioComissaoStrategy {
	
	
	private Double adicionalGerente(){
		return ((tempoDeCasaEmAnos() * 5) / 100) * getSalario();
	}
	
	
}

		</pre>
	</div>
	
	
	<br>
	<p>Passo 9 - Implementar o detalhe da regra no novo m�todo da classe
		CalcularSalarioComissaoStrategyGerente no m�todo calcularComissao, e
		ajustar o m�todo auxiliar para que compile, ao final dever� ficar como
		o exemplo abaixo.</p>
	
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
	
package refatoracao.strategy;

import refatoracao.pojo.Funcionario;

public class CalcularSalarioComissaoStrategyGerente 
				extends CalcularSalarioComissaoStrategy {

	@Override
	Double calcularComissao(Funcionario funcionario) {
		return (funcionario.getSalario() * 0.3) 
				+ adicionalGerente(funcionario.tempoDeCasaEmAnos(), 
							funcionario.getSalario());
	}

	
	private Double adicionalGerente(int tempoDeCasa,double salario){
		return ((tempoDeCasa * 5) / 100) * salario;
	}

}
	
		</pre>
	</div>
	
	
	<br>
	<p>Passo 10 - Implementar o detalhe da regra no novo m�todo da
		classe CalcularSalarioComissaoStrategyCoodenador.</p>
	
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
	
package refatoracao.strategy;

import refatoracao.pojo.Funcionario;

public class CalcularSalarioComissaoStrategyCoordenador 
					extends CalcularSalarioComissaoStrategy {

	@Override
	Double calcularComissao(Funcionario funcionario) {
		return (funcionario.getSalario() * 0.2);
	}

}
	
	
		</pre>
	</div>
	
	<br>
	<p>Passo 11 - Implementar o detalhe da regra no novo m�todo da
		classe CalcularSalarioComissaoStrategyComum.</p>
	
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
	
package refatoracao.strategy;

import refatoracao.pojo.Funcionario;

public class CalcularSalarioComissaoStrategyComum 
					extends CalcularSalarioComissaoStrategy {

	@Override
	Double calcularComissao(Funcionario funcionario) {
		return (funcionario.getSalario() * 0.1);
	}

}

		</pre>
	</div>
	
	<br>
	<p>Passo 12 - Ajustar classe CalcularSalarioComissaoStrategy e usar
		polimorfismo para calcular a comiss�o.</p>
	
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
	
package refatoracao.strategy;

import refatoracao.pojo.Funcionario;

public abstract class CalcularSalarioComissaoStrategy {
	
	abstract Double calcularComissao(Funcionario funcionario);
	
	public Double calcularSalarioComComissao(Funcionario funcionario){
		
		return funcionario.getSalario() + calcularComissao(funcionario); 
			
	}
}


		</pre>
	</div>
	
	<br>
	<p>Passo 13 - Criar um atributo do tipo	CalcularSalarioComissaoStrategy 
		para ser usado na classe de calculo do	contexto Funcionario. 
		No exemplo abaixo ele � o primeiro dos atributos e o m�todo
		calcular � o ultimo.</p>
	
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">

public class Funcionario {

	private CalcularSalarioComissaoStrategy calculoStrategy;

	...
	
	public Double calcularSalarioComComissao(){
	
		return calculoStrategy.calcularSalarioComComissao(this);
		
	}
	
}

		</pre>
	</div>
	
	<br>
	<p>Passo 14 - Criaremos um contrutor no contexto que tenha como
		parametro a inst�ncia de CalcularSalarioComissaoStrategy que desejamor
		usar junto com os demais atributos. O contrutor deve ser implementado 
		na classe relacionada ao exempro anterior.</p>
	
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
//Construtor com a instancia da estrategia que queremos usar.
public Funcionario (Pessoa pessoa, TipoFuncionario tipoFuncionario, 
					Salario salario, Calendar admissao, 
					CalcularSalarioComissaoStrategy calculoStrategy){
	this.pessoa = pessoa;
	this.tipoFuncionario = tipoFuncionario;
	this.salario = salario;
	this.calculoStrategy = calculoStrategy;
}

		</pre>
	</div>
	
	<br>
	<p>Passo 15 - Ajustar teste para utilizar nova estrutura e validar o retorno.
		Abaixo exemplo de como deve ficar a classe de teste. Repare que no 
		construtor do contexto � passada a instancia desejada para ser usada 
		no contexto.</p>
	
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
	
public class TesteCalculoSalario {

	@Test
	public void test() {
		Pessoa pessoa = new Pessoa();
		Calendar nascimento = Calendar.getInstance();
		nascimento.set(1980, 10, 15);
		
		pessoa.setDocumento("123456789");
		pessoa.setNascimento(nascimento);
		pessoa.setNome("Juliana Maria");
		
		Calendar admissao = Calendar.getInstance();
		admissao.set(2010, 10, 15);
		Salario salario = new Salario();
		salario.setValorSalario(11000.00);
		
		//usamos o strategy de coordenador.
		Funcionario funcionario = 
			new Funcionario(pessoa,TipoFuncionario.COORDENADOR,
				salario,admissao, 
				new CalcularSalarioComissaoStrategyCoordenador());

		Assert.assertTrue(funcionario.calcularSalarioComComissao()
					.equals(new Double(13200.00)));
	}
}
	
		</pre>
	</div>
	
	<!-- Button trigger modal -->
<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal"
style="position:fixed;bottom: 50px;right: 50px;">
  Exibir Problema
</button>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
      <mc:excessodelogicacondicional/>
    <div class="modal-content">
<!--       <div class="modal-header"> -->
<!--         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
<!--         <h4 class="modal-title" id="myModalLabel">Mau Cheiro</h4> -->
<!--       </div> -->
<!--       <div class="modal-body"> -->
<!--       </div> -->
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>
</div>
<script src="prettify/prettify_run.js"></script>