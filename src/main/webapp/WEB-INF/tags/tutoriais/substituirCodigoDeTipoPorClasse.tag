<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@taglib tagdir="/WEB-INF/tags/mauscheiros" prefix="mc"%>
<div style="margin-left: 25px;">
	<h2>Tutorial Refatora��o</h2>

	<h3>Substituir c�digo de tipo por classe</h3>

	<p>Caso queria acompanhar esse tutorial usando o mesmo contexo
		basta clicar no link, fazer o download do projeto e buscar a
		refatora��o deste tutorial.</p>

	<a
		href="https://luizfernando_andrade@bitbucket.org/luizfernando_andrade/torefactor.git">
		projeto_para_download</a> <br> <br> <br>

	<p>Passo 1 - Ap�s identificar os atributos de tipo invari�vel,
		criar uma enumera��o que os represente.</p>
	<br>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">

public enum EnumSituacao {
	
}
		</pre>
	</div>
	<br>
	<p>Passo 2 - Mover todos os atributos para o EnumSituacao.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public enum EnumSituacao {

	SEPARACAO, 
	TRANSPORTADORA,
	CORREIOS,
	ENTREGUE;

}
		</pre>
	</div>


	<br>
	<p>Passo 3 - Configurar os valores do enum com a mesma sa�da que
		possuiam.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">

public enum EnumSituacao {


	SEPARACAO("Separacao"), 
	TRANSPORTADORA("Transportadora"),
	CORREIOS("Correios"),
	ENTREGUE("Entregue");
	
	private String string;

	private EnumSituacao(String string) {
		this.string = string;
	}
	
	public String toString(){
		return string;
	}
	
}	
		</pre>
	</div>
	<br>
	<p>Passo 4 - Criar um atributo do tipo EnumSituacao na classe
		cliente e remover demais atributos que manipulavam a situa��o do
		objeto.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public class Encomenda {

	EnumSituacao situacao;
	
	...
}	
		</pre>
	</div>
	<br>
	<p>Passo 5 - Modificar a classe para usar o Enum ao inv�z dos
		atributos de tipo.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public class Encomenda {

	EnumSituacao situacao;
	
	public EnumSituacao mudarSituacao(){
		if (situacao.equals(EnumSituacao.SEPARACAO)){
			situacao = EnumSituacao.TRANSPORTADORA;
		} else if (situacao.equals(EnumSituacao.TRANSPORTADORA)) {
			situacao = EnumSituacao.CORREIOS;
		} else {
			situacao = EnumSituacao.ENTREGUE;
		}
		return situacao;
	}
	
}
		</pre>
	</div>

	<br>
	<p>Passo 6 - Validar a execu��o com teste de unidade, ajustando
		para a nova estrutura.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public class TestarMudancaEstado {

	@Test
	public void mudarEstado(){
		Encomenda encomenda = new Encomenda();
		
		encomenda.setSituacao(EnumSituacao.SEPARACAO);
		
		encomenda.mudarSituacao();
		
		Assert.assertTrue(encomenda.getSituacao()
				.equals(EnumSituacao.TRANSPORTADORA));
	}
}
		</pre>
	</div>


	<!-- Button trigger modal -->
	<button type="button" class="btn btn-primary btn-lg"
		data-toggle="modal" data-target="#myModal"
		style="position: fixed; bottom: 50px; right: 50px;">Exibir
		Problema</button>

	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<mc:excessoDeAtributoComValorFixo/>
			<div class="modal-content">
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="prettify/prettify_run.js"></script>