<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@taglib tagdir="/WEB-INF/tags/mauscheiros" prefix="mc"%>
<div style="margin-left: 25px;">
	<h2>Tutorial Refatora��o</h2>

	<h3>Substituir �rvore impl�cita por Composite</h3>

	<p>Caso queria acompanhar esse tutorial usando o mesmo contexo
		basta clicar no link, fazer o download do projeto e buscar a
		refatora��o deste tutorial.</p>

	<a
		href="https://luizfernando_andrade@bitbucket.org/luizfernando_andrade/torefactor.git">
		projeto_para_download</a> <br> <br> <br>

	<p>Passo 1 - Ap�s identificar a �rvore impl�cita, localizar uma das
		folhas da �rvore e aplicar a refatora��o extrair m�todo.</p>
	<br>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
		
public class Expressao {

	HashMap&lt;String, Double&gt; eixoX = new HashMap&lt;&gt;();
	
	public Map&lt;String, Double&gt; calcularBhaskara(Double a, Double b, Double c){
		
		eixoX.put("x1", ((-b + Math.sqrt( calcularDelta(a, b, c)))/2*a));
		eixoX.put("x2", ((-b - Math.sqrt( calcularDelta(a, b, c)))/2*a));
		
		return eixoX;
	}

	private double calcularDelta(Double a, Double b, Double c) {
		return Math.pow(b, 2) - (4*a*c);
	}
	
}
		</pre>
	</div>
	<br>
	<p>Passo 2 - Criar uma classe Composite referente ao n� do novo
		m�todo.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
		
public class DeltaComposite {

	public DeltaComposite(){}
	
}
		</pre>
	</div>


	<br>
	<p>Passo 3 - Aplicar a refatora��o mover m�todo para a nova classe
		Composite. e criar os atributos necess�rios para a execu��o dos
		processos</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">

public class DeltaComposite {
	
	private Double a;
	private Double b;
	private Double c;

	public DeltaComposite(Double a, Double b, Double c){
		this.a = a;
		this.b = b;
		this.c = c;
	}
	
	public double calcularDelta() {
		return Math.pow(b, 2) - (4*a*c);
	}
}
		</pre>
	</div>


	<br>

	<p>Passo 4 - Criar uma inst�ncia da nova classe composite na classe
		cliente, passando os atributos necess�rios no construtor.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
		
public class Expressao {

	HashMap&lt;String, Double&gt; eixoX = new HashMap&lt;&gt;();
	
	public Map&lt;String, Double&gt; calcularBhaskara(Double a, Double b, Double c){
		
		DeltaComposite delta = new DeltaComposite(a, b, c);
		
		eixoX.put("x1", ((-b + Math.sqrt( delta.calcularDelta()))/2*a));
		eixoX.put("x2", ((-b - Math.sqrt( delta.calcularDelta()))/2*a));
		
		return eixoX;
	}
	
}	
		</pre>
	</div>

	<br>
	<p>Passo 5 - Identificar o n� pai da �rvore e aplicar refatora��o
		extrair m�todo. Nomear o m�todo com nome sujestivo para a fun��o.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">

public class Expressao {

	HashMap&lt;String, Double&gt; eixoX = new HashMap&lt;&gt;();
	
	public Map&lt;String, Double&gt; calcularBhaskara(Double a, Double b, Double c){
		
		DeltaComposite delta = new DeltaComposite(a, b, c);
		
		eixoX.put("x1", calcularBhaskaraX1(a, b, delta));
		eixoX.put("x2", ((-b - Math.sqrt( delta.calcularDelta()))/2*a));
		
		return eixoX;
	}

	private double calcularBhaskaraX1(Double a, Double b, DeltaComposite delta) {
		return (-b + Math.sqrt( delta.calcularDelta()))/2*a;
	}
	
}
		</pre>
	</div>


	<br>
	<p>Passo 6 - Aplicar a refatora��o para demais n�s.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public class Expressao {

	HashMap&lt;String, Double&gt; eixoX = new HashMap&lt;&gt;();
	
	public Map&lt;String, Double&gt; calcularBhaskara(Double a, Double b, Double c){
		
		DeltaComposite delta = new DeltaComposite(a, b, c);
		
		eixoX.put("x1", calcularBhaskaraX1(a, b, delta));
		eixoX.put("x2", calcularBhaskaraX2(a, b, delta));
		
		return eixoX;
	}

	private double calcularBhaskaraX2(Double a, Double b, DeltaComposite delta) {
		return (-b - Math.sqrt( delta.calcularDelta()))/2*a;
	}

	private double calcularBhaskaraX1(Double a, Double b, DeltaComposite delta) {
		return (-b + Math.sqrt( delta.calcularDelta()))/2*a;
	}
	
}
		</pre>
	</div>

	<br>
	<p>Passo 7 - Criar uma classe nova para cada, n� da �rvore
		impl�cita.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public class BhaskaraX2 {

}

// Arquivos diferentes


public class BhaskaraX2 {

}

		</pre>
	</div>

	<br>
	<p>Passo 8 - Mover os novos m�todos para suas respectivas classes.
		Criar os atributos que forem necess�rios � classe e passar como
		par�metro no construtor. Aplicar refatora��o para n�s semelhantes</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">

public class BhaskaraX1 {
	
	private DeltaComposite delta;
	private Double a;
	private Double b;

	public BhaskaraX1(Double a, Double b, DeltaComposite delta) {
		this.a = a;
		this.b = b;
		this.delta = delta;
	}
	
	public double calcularBhaskaraX1() {
		return (-b + Math.sqrt( delta.calcularDelta()))/2*a;
	}
}
		</pre>
	</div>
	<br>
	<p>Passo 9 - Criar inst�ncias das classes rec�m criadas na classe
		cliente e ajustar para usar os m�todos necess�rios.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">

public class Expressao {

	HashMap&lt;String, Double&gt; eixoX = new HashMap&lt;&gt;();
	
	public Map&lt;String, Double&gt; calcularBhaskara(Double a, Double b, Double c){
		
		DeltaComposite delta = new DeltaComposite(a, b, c);
		BhaskaraX1 x1 = new BhaskaraX1(a, b, delta);
		BhaskaraX2 x2 = new BhaskaraX2(a, b, delta);
		eixoX.put("x1", x1.calcularBhaskaraX1());
		eixoX.put("x2", x2.calcularBhaskaraX2());
		
		return eixoX;
	}
	
}
		</pre>
	</div>
	<br>
	<p>Passo 10 - Validar resultados executando os testes relacionados
		� classe cliente.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">

public class TestarFuncao {

	@Test
	public void testarBhaskaraX1(){
		Expressao bhaskara = new Expressao();
		HashMap&lt;String, Double&gt; resultado = (HashMap&lt;String, Double&gt;) 
				bhaskara.calcularBhaskara(1.0, -2.0, -3.0);
		
		Double x1 = resultado.get("x1");
		
		System.out.println("X1 = " + x1 );
		
		Assert.assertTrue(x1 == 3.0);
	}
	@Test
	public void testarBhaskaraX2(){
		Expressao bhaskara = new Expressao();
		HashMap&lt;String, Double&gt; resultado = (HashMap&lt;String, Double&gt;) 
				bhaskara.calcularBhaskara(1.0, -2.0, -3.0);
		
		Double x2 = resultado.get("x2");
		
		System.out.println("X2 = " + x2 );
		
		Assert.assertTrue(x2 == -1.0);
	}
}
		</pre>
	</div>

	<!-- Button trigger modal -->
	<button type="button" class="btn btn-primary btn-lg"
		data-toggle="modal" data-target="#myModal"
		style="position: fixed; bottom: 50px; right: 50px;">Exibir
		Problema</button>

	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<mc:rastreabilidadeEstruturaComplexa />
			<div class="modal-content">
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="prettify/prettify_run.js"></script>