<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@taglib tagdir="/WEB-INF/tags/mauscheiros" prefix="mc"%>
<div style="margin-left: 25px;">
	<h2>Tutorial Refatora��o</h2>

	<h3>Substituir Notifica��es Hard-Code por Observer</h3>

	<p>Caso queria acompanhar esse tutorial usando o mesmo contexo
		basta clicar no link, fazer o download do projeto e buscar a
		refatora��o deste tutorial.</p>

	<a
		href="https://luizfernando_andrade@bitbucket.org/luizfernando_andrade/torefactor.git">
		projeto_para_download</a> <br> <br> <br>

	<p>Passo 1 - Aplicar a refatora��o extrair interface da classe que
		precisa ser notificada sobre a mudan�a de estado de um objeto de outra
		classe, no caso a classe ControleBiblioteca. Coloque um nome sujestivo
		na interface, foi adotado Observador.</p>
	<br>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
package refatoracao.biblioteca.pojo;

public interface ObservarLivro {
	

}

		</pre>
	</div>
	<br>
	<p>Passo 2 - Assinar o m�todo notificar na interface criada e
		assinar o m�todo notificar, recebendo como par�metro a classe que ser�
		observada.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">

package refatoracao.biblioteca.pojo;

public interface ObservarLivro {
	
	public String notificar(Livro livro);

}
		</pre>
	</div>


	<br>
	<p>Passo 3 - Na classe filha da nova interfaca, implementar o
		m�todo de notifica��o para que capture a notifica��o e adcione na
		mensagem.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">

public class ControleBiblioteca {

...
	@Override
	public String notificar(Livro livro) {
		if (EstadoLivro.reservado.equals(livro.getEstadoLivro())){
			log = "Livro possui reserva";
			System.out.println(log);
		} else {
			log = "Livro liberado"; 
		}
		System.out.println(log);
		return log;
	}
}
		
		</pre>
	</div>

	<br>
	<p>Passo 4 - Na classe que ser� observada crie um atributo do tipo
		lista da nova interface.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
		
public class Livro {
...
	List&lt;ObservarLivro&gt; observers = new ArrayList&lt;&gt;();
...
}
		</pre>
	</div>


	<br>
	<p>Passo 5 - Implemente um m�todo que adicione um objeto por vez na
		lista de objetos do tipo Observador, para que a classe possa ser
		observada por mais de um observador.</p>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public class Livro {

List&lt;ObservarLivro&gt; observers = new ArrayList&lt;&gt;();
...
	public void adicionarObservador(ObservarLivro observador){
		observers.add(observador);
	}
...
}

		</pre>
	</div>

	<br>
	<p>Passo 6 - Implemente um m�todo que remova um objeto por vez da
		lista de observadores, para caso haja a necessidade de remover um
		observador da lista.</p>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">

public class Livro {

List&lt;ObservarLivro&gt; observers = new ArrayList&lt;&gt;();
...
	public void adicionarObservador(ObservarLivro observador){
		observers.add(observador);
	}
	
	public void removerObservador(ObservarLivro observador){
		observers.remove(observador);
	}
	
...
}

		</pre>
	</div>


	<br>
	<p>Passo 7 - Implementar um m�todo que varra a lista de
		observadores e chame o m�todo notificar referente a cada inst�ncia,
		passando a classe observada como par�metro.</p>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public class Livro {

List&lt;ObservarLivro&gt; observers = new ArrayList&lt;&gt;();
...
	public void adicionarObservador(ObservarLivro observador){
		observers.add(observador);
	}
	
	public void removerObservador(ObservarLivro observador){
		observers.remove(observador);
	}
	
	public void notificarObservadores(){
		for (ObservarLivro observarLivro : observers) {
			observarLivro.notificar(this);
		}
	}
...
}

		</pre>
	</div>
	<br>
	<p>Passo 8 - substituir a instancia da classe de notifica��o pelo
		m�todo de notifica��o implementado na pr�pria classe observada.</p>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public class Livro {
...
	//m�todo antes da modifica��o
	public void devolverLivro(){
		notificacao.notifica(this);
		setEstadoLivro(EstadoLivro.liberado);
	}
	
	//m�todo ap�s a altera��o
	public void devolverLivro(){
		notificarObservadores();
		setEstadoLivro(EstadoLivro.liberado);
	}

}
		</pre>
	</div>

	<br>

	<p>Passo 9 - Remover a classe observadora e ajustar o teste para
		que receba a instancia do observador. Ap�s ajuste basta executar o
		teste.</p>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">

public class TesteEmprestimo {

	@Test
	public void testarEmprestimo(){
		//preparar teste
		Livro livro = new Livro();
		livro.setEstadoLivro(EstadoLivro.reservado);
		livro.setTitulo("LivroTeste");
		
		livro.adicionarObservador(new ControleBiblioteca());
		
		//testar devolu��o
		livro.devolverLivro();
		
		Assert.assertTrue(EstadoLivro.liberado.equals(livro.getEstadoLivro()));
	}
}
		</pre>
	</div>




	<!-- Button trigger modal -->
	<button type="button" class="btn btn-primary btn-lg"
		data-toggle="modal" data-target="#myModal"
		style="position: fixed; bottom: 50px; right: 50px;">Exibir
		Problema</button>

	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<mc:classeObservadoraHierarquia/>
			<div class="modal-content">
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="prettify/prettify_run.js"></script>