<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@taglib tagdir="/WEB-INF/tags/mauscheiros" prefix="mc"%>
<div style="margin-left: 25px;">
	<h2>Tutorial Refatora��o</h2>

	<h3>Mover acumula��o para Par�metro Coletor</h3>

	<p>Caso queria acompanhar esse tutorial usando o mesmo contexo
		basta clicar no link, fazer o download do projeto e buscar a
		refatora��o deste tutorial.</p>

	<a
		href="https://luizfernando_andrade@bitbucket.org/luizfernando_andrade/torefactor.git">
		projeto_para_download</a> <br> <br> <br>

	<p>Passo 1 - identificar onde o objeto que coleta as informa��es �
		modificado e aplicar a refatora��o extrair m�todo.</p>
	<br>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public class MontarPedido {
	public String detalharPedido(){
		pedido.append(saborPizza);
		
		//m�todo de acumula��o
		pizzaComDoisRecheios(); 
		
		if(bordaChedar){
			pedido.append(" com borda de chedar");
		} 
		if (tamanho > 45) {
			pedido.append(" 45cm + 1 pizza m�dia gr�tis");
		} else {
			pedido.append(tamanho.toString());
		}
		return pedido.toString();

	}
	private void pizzaComDoisRecheios() {
		if(doisRecheios){
			pedido.append(" metade: ")
			.append(saborPizza2);
		}
	}
}
		</pre>
	</div>
	<br>
	<p>Passo 2 - Ajustar todas as ocorr�ncias criando novos m�todos de
		acumula��o.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">

public class MontarPedido {
...
	public String detalharPedido(){
		pedido.append(saborPizza);
		
		pizzaComDoisRecheios(); 
		
		possuiBorda(); 
		
		return promocaoTamanho();

	}
}
		</pre>
	</div>


	<br>
	<p>Passo 3 - Compilar e testar para certificar-se de que tudo
		continua como antes.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
		
public class TestarPedido {

	@Test
	public void pedidoDoisSabores50Cm(){
		MontarPedido pedido = new MontarPedido();
		pedido.setBordaChedar(true);
		pedido.setDoisRecheios(true);
		pedido.setSaborPizza("Marguerita");
		pedido.setSaborPizza2("Peperoni");
		pedido.setTamanho(50);
		
		String detalhamento = pedido.detalharPedido();
		
		Assert.assertTrue(detalhamento.contains("1 pizza m�dia gr�tis"));
	}
}
		
		</pre>
	</div>


	<!-- Button trigger modal -->
	<button type="button" class="btn btn-primary btn-lg"
		data-toggle="modal" data-target="#myModal"
		style="position: fixed; bottom: 50px; right: 50px;">Exibir
		Problema</button>

	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<mc:objetosquecolheminformacao/>
			<div class="modal-content">
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="prettify/prettify_run.js"></script>