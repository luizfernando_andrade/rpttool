<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@taglib tagdir="/WEB-INF/tags/mauscheiros" prefix="mc"%>
<div style="margin-left: 25px;">
	<h2>Tutorial Refatora��o</h2>

	<h3>Introduzir Cria��o Polim�rfica com Factory Method</h3>

	<p>Caso queria acompanhar esse tutorial usando o mesmo contexo
		basta clicar no link, fazer o download do projeto e buscar a
		refatora��o deste tutorial.</p>

	<a
		href="https://luizfernando_andrade@bitbucket.org/luizfernando_andrade/torefactor.git">
		projeto_para_download</a> <br> <br> <br>

	<p>Passo 1 - Validar se a estrutura dos m�todos possui diferen�a
		apenas na instancia��o dos objetos. No exemplo, tanto a classe
		TestDriverChrome quanto a classe TestDriverFirefox implementam o mesmo
		m�todo, diferindo apenas a l�gica de cria��o.</p>
	<br>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public class TestDriverChrome extends TestDriver {

	@Override
	public String direcionarURL(String url) {
		
		WebDriver driver = new GoogleDriver();
		
		driver.setUrl(url);
		
		return driver.abrirBrowser();
	}
}
		</pre>
	</div>
	<br>
	<p>Passo 2 - Extrair a L�gica de cria��o para um m�todo gen�rico,
		esse novo m�todo de cria��o tamb�m ser� usado nas demais classes
		irm�s. Ap�s aplicar a refatora��o Extrair M�todo a classe ficar� como
		segue.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public class TestDriverChrome extends TestDriver {

	@Override
	public String direcionarURL(String url) {
		
		WebDriver driver = newDriver();
		
		driver.setUrl(url);
		
		return driver.abrirBrowser();
	}

	private WebDriver newDriver() {
		WebDriver driver = new GoogleDriver();
		return driver;
	}

}
		</pre>
	</div>


	<br>
	<p>passo 3 - Modificar todas as classes que utilizam o m�todo que
		cont�m a l�gica de cria��o, de acordo com o passo 2. Agora as classes
		possuem a mesma estrutura.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public class TestDriverFirefox extends TestDriver{

	@Override
	public String direcionarURL(String url) {
		
		WebDriver driver = newDriver();
		
		driver.setUrl(url);
		
		return driver.abrirBrowser();
	}

	private WebDriver newDriver() {
		WebDriver driver = new FirefoxDriver();
		return driver;
	}
}
		</pre>
	</div>

	<br>
	<p>passo 4 - Subir o m�todo de c�digo duplicado na hierarquia, caso
		n�o possa modificar a classe pai crie uma intermediaria que contenha o
		conte�do da classe mais alta e fa�a as modifica��es a seguir, neste
		caso n�o foi necess�rio por � poss�vel modificar a classe pai. A
		classe deve ficar como exemplo.</p>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public abstract class TestDriver {

	
	public String direcionarURL(String url) {
		
		WebDriver driver = newDriver();
		
		driver.setUrl(url);
		
		return driver.abrirBrowser();
	}
	
}
		</pre>
	</div>
	
	<br>
	<p>Passo 5 - Para que o m�todo movido compile, � necess�rio criar a
		assinatura do m�todo de cria��o na classe. Aplicar a refatora��o,
		Formar Template Method pode auxiliar. A classe deve ficar como segue.
	</p>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public abstract class TestDriver {

	protected abstract WebDriver newDriver();
	
	public String direcionarURL(String url) {
		
		WebDriver driver = newDriver();
		
		driver.setUrl(url);
		
		return driver.abrirBrowser();
	}
}
		</pre>
	</div>
	<br>
	<p>Passo 6 - Remover a l�gica duplicada nas classes filhas.</p>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public class TestDriverChrome extends TestDriver {
...
	protected WebDriver newDriver() {
		WebDriver driver = new GoogleDriver();
		return driver;
	}
...
}

public class TestDriverFirefox extends TestDriver{
...
	protected WebDriver newDriver() {
		WebDriver driver = new FirefoxDriver();
		return driver;
	}
...
}
		</pre>
	</div>
	<br>
	<p>Passo 7 - Ap�s as modifica��es, basta aplicar os teste e validar
		a sa�da, que deve continuar a mesma por�m o c�digo est� mais limpo e
		sem duplica��o.</p>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public class TestarWebDriver {

	@Test
	public void testarDriverChrome(){
		
		TestDriverChrome driver = new TestDriverChrome();
		
		String browser = driver.direcionarURL("http://www.google.com");
		
		Assert.assertTrue(browser.contains("Chrome"));
	}
	@Test
	public void testarDriverFirefox(){
		
		TestDriverFirefox driver = new TestDriverFirefox();
		
		String browser = driver.direcionarURL("http://www.google.com");
		
		Assert.assertTrue(browser.contains("Firefox"));
	}
}
		</pre>
	</div>
	
	<!-- Button trigger modal -->
	<button type="button" class="btn btn-primary btn-lg"
		data-toggle="modal" data-target="#myModal"
		style="position: fixed; bottom: 50px; right: 50px;">Exibir
		Problema</button>

	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<mc:construtoresComunsEmClassesFilhas/>
			<div class="modal-content">
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="prettify/prettify_run.js"></script>