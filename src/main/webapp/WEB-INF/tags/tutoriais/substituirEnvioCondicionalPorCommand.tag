<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@taglib tagdir="/WEB-INF/tags/mauscheiros" prefix="mc"%>
<div style="margin-left: 25px;">
	<h2>Tutorial Refatora��o</h2>

	<h3>Substituir envio condicional por Command</h3>

	<p>Caso queria acompanhar esse tutorial usando o mesmo contexo
		basta clicar no link, fazer o download do projeto e buscar a
		refatora��o deste tutorial.</p>

	<a
		href="https://luizfernando_andrade@bitbucket.org/luizfernando_andrade/torefactor.git">
		projeto_para_download</a> <br> <br> <br>

	<p>Passo 1 - Ap�s a identifica��o do mau cheiro a ser refatorado
		para o padr�o Command, criar uma interface Command com nome sugestivo
		a a��o que ser� tratada.</p>
	<br>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
	
package refatoracao.validacao.pojo;

public interface ValidarArquivoCommand {

}

		</pre>
	</div>
	<br>
	<p>Passo 2 - Na interface command defina o m�todo executar para que
		todas as classes que implementarem esta interface assinem esse
		contrato.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
package refatoracao.validacao.pojo;

public interface ValidarArquivoCommand {

	public String executar();
}

		</pre>
	</div>


	<br>
	<p>Passo 3 - Para cada condicional existente, crie uma classe que
		represente sua a��o e fa�a com que implementem a interface command. No
		exemplo foi mostrado a classe ValidarArquivoComercial, repetir o mesmo
		para ArquivoFinanceiro e ArquivoEstoque.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public class ValidarArquivoComercial implements ValidarArquivoCommand {

	public String executar() {
	
	}
}
		</pre>
	</div>


	<br>

	<p>Passo 4 - Aplicar a refatora��o, extrair m�todo, nomeio como
		executar na classe cliente, ControlePostoGasolina.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public class ControlePostoGasolina {
...
	public String validarArquivo(){
		return executar();
	}
	private String executar() {
		if (TipoArquivo.Financeiro.equals(arquivo.getTipoArquivo())){
			ArquivoFinanceiro financeiro = new ArquivoFinanceiro();
			financeiro.adicionarValorTotalFinanceiro(arquivo.getImpostosPagos());
			arquivo.setResponsavel(financeiro.setorResponsavel());
			return arquivo.getResponsavel();
		}
		if (TipoArquivo.Comercial.equals(arquivo.getTipoArquivo())){
			ArquivoComercial comercial = new ArquivoComercial();
			comercial.adicionarValorTotalComercial(arquivo.getImpostosPagos());
			arquivo.setResponsavel(comercial.setorResponsavel());
			return arquivo.getResponsavel();
		} else {
			ArquivoEstoque estoque= new ArquivoEstoque();
			estoque.adicionarValor(arquivo.getImpostosPagos());
			arquivo.setResponsavel(estoque.setorResponsavel());
			return arquivo.getResponsavel();
		}
	}
}
		</pre>
	</div>

	<br>
	<p>passo 5 - Adicione no m�todo da interface os par�metros
		necess�rios para a compila��o do c�digo, j� que todos os condicionais
		usaram o mesmo par�metro, no caso deste deste exemplo o tipo
		ArquivoDeIntegracao</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public class ValidarArquivoComercial implements ValidarArquivoCommand {

	public String executar(ArquivoDeIntergracao arquivo);
	
	}
}
		</pre>
	</div>

	<br>
	<p>Passo 6 - Mova cada l�gica contina nos condicionais para o
		m�todo executar() de sua classe de refer�ncia. O exemplo mostra a
		classe ValidarArquivoEstoque, e deve seguir o mesmo modelo para as
		demais classes.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public class ValidarArquivoEstoque implements ValidarArquivoCommand {

	@Override
	public String executar(ArquivoDeIntergracao arquivo) {
		ArquivoEstoque estoque= new ArquivoEstoque();
		estoque.adicionarValor(arquivo.getImpostosPagos());
		arquivo.setResponsavel(estoque.setorResponsavel());
		return arquivo.getResponsavel();
	}

}
		</pre>
	</div>

	<br>
	<p>Passo 7 - Na classe cliente, no caso ControlePostoGasolina,
		criar um atributo do tipo ValidarArquivoCommand para que possa acessar
		ao m�todo executar da classe necess�ria.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
	
public class ControlePostoGasolina {

...
	ValidarArquivoCommand validarArquivo;
	
	public ControlePostoGasolina(ArquivoDeIntergracao arquivo) {
		this.arquivo = arquivo;
	}
...
}
	
		</pre>
	</div>


	<br>
	<p>Passo 8 - Ajuste o m�todo da classe validar para acessar o
		m�todo do objeto command criado na classe e passando o par�metro
		necess�rio.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
	

public class ControlePostoGasolina {

...
	public String validarArquivo(){
		return validarArquivo.executar(arquivo);
	}
...
}

		</pre>
	</div>


	<br>
	<p>Passo 9 - No construtor da classe cliente, receber um atributo
		do tipo Command e atribuir ao atributo da classe, dessa forma ser�
		acessado o m�todo que for passado como par�metro.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
	
public class ControlePostoGasolina {

	ArquivoDeIntergracao arquivo;
	ValidarArquivoCommand validarArquivo;
	
	public ControlePostoGasolina(ArquivoDeIntergracao arquivo,ValidarArquivoCommand validarArquivo) {
		this.arquivo = arquivo;
		this.validarArquivo = validarArquivo;
	}
	...
}
	
		</pre>
	</div>


	<br>
	<p>Passo 10 - Remover o m�todo executar() da classe cliente,
		ControlePostoGasolina. A classe deve ficar como exemplo apresentado.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">

public class ControlePostoGasolina {

	ArquivoDeIntergracao arquivo;
	ValidarArquivoCommand validarArquivo;
	
	public ControlePostoGasolina(ArquivoDeIntergracao arquivo,ValidarArquivoCommand validarArquivo) {
		this.arquivo = arquivo;
		this.validarArquivo = validarArquivo;
	}
	
	public String validarArquivo(){
		return validarArquivo.executar(arquivo);
	}
}
	
		</pre>
	</div>

	<br>
	<p>Passo 11 - Ajustar os locais onde � chamado o contrutor da classe
		cliente para passar o objeto de Command necess�rio para a valida��o. O
		teste deve continuar com a mesma sa�da. Segue exemplo do teste.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public class TestarTransmissaoDeArquivo {

	@Test
	public void testarTransmissaoFinanceiro(){
		ArquivoDeIntergracao arquivo = new ArquivoDeIntergracao();
		arquivo.setImpostosPagos(35.0);
		arquivo.setResponsavel("Gerente Financeiro");
		arquivo.setTipoArquivo(TipoArquivo.Financeiro);
		arquivo.setImpostosPagos(200.0);
		
		ControlePostoGasolina posto = new ControlePostoGasolina(arquivo,
						 new ValidarArquivoFinanceiro());
		String validadoPor = posto.validarArquivo();
		System.out.println(validadoPor);
		
		Assert.assertTrue(validadoPor.contains("Financeiro"));
		
	}
}
		</pre>
	</div>


	<!-- Button trigger modal -->
	<button type="button" class="btn btn-primary btn-lg"
		data-toggle="modal" data-target="#myModal"
		style="position: fixed; bottom: 50px; right: 50px;">Exibir
		Problema</button>

	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<mc:condicionaisProjetamAcao/>
			<div class="modal-content">
				<!--       <div class="modal-header"> -->
				<!--         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
				<!--         <h4 class="modal-title" id="myModalLabel">Mau Cheiro</h4> -->
				<!--       </div> -->
				<!--       <div class="modal-body"> -->
				<!--       </div> -->
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="prettify/prettify_run.js"></script>