<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@taglib tagdir="/WEB-INF/tags/mauscheiros" prefix="mc"%>
<div style="margin-left: 25px;">
	<h2>Tutorial Refatora��o</h2>

	<h3>Mover acumula��o para Visitor</h3>

	<p>Caso queria acompanhar esse tutorial usando o mesmo contexo
		basta clicar no link, fazer o download do projeto e buscar a
		refatora��o deste tutorial.</p>

	<a
		href="https://luizfernando_andrade@bitbucket.org/luizfernando_andrade/torefactor.git">
		projeto_para_download</a> <br> <br> <br>

	<p>Passo 1 - Ap�s a identifica��o do m�todo de acumula��o, caso existam
		vari�veis locais, � necess�rio tornalas atributos da classe. Tanto 
		a variavel do tipo StringBuffer quanto a do tipo String foram extraidas 
		do m�todo.</p>
	<br>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
	
public class RelatorioEnfermaria implements Relatorio {

	StringBuffer sb = new StringBuffer();
	String resultado = null;
	
	public String imprimir(TipoArquivo tipoArquivo){
		
		if (tipoArquivo instanceof TipoArquivoHTML){
			
			sb.append("&lt;head&gt;" + getCabecalho() + "&lt;/head&gt;");
			sb.append("&lt;body&gt;" + getCorpo() + "&lt;/body&gt;");
			sb.append("&lt;footer&gt;" + getRodape() + "&lt;/footer&gt;");
			
			resultado = tipoArquivo.imprimir(sb.toString());
		}
		if (tipoArquivo instanceof TipoArquivoXML) {
			
			sb.append("&lt;cabecalho&gt;" + getCabecalho() + "&lt;/cabecalho&gt;");
			sb.append("&lt;corpo&gt;" + getCorpo() + "&lt;/corpo&gt;");
			sb.append("&lt;rodape&gt;" + getRodape() + "&lt;/rodape&gt;");
			resultado = tipoArquivo.imprimir(sb.toString());
		}
		return resultado; 
		
	}

		</pre>
	</div>
	<br>
	<p>Passo 2 - Mover a l�gica de cada acumula��o, para classes onde
		as informa��es ser�o acumuladas, fazendo com que o m�todo que contenha
		a l�gica, e receba o hospedeiro como par�metro, ajustando onde for necess�rio.
		Nomeie o m�todo para "aceitar".</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
public class TipoArquivoHTML implements TipoArquivo {
...
	public void aceitar(RelatorioEnfermaria relatorio){
		relatorio.getSb()
			.append("&lt;head&gt;" + relatorio.getCabecalho() + "&lt;/head&gt;");
		relatorio.getSb()
			.append("&lt;body&gt;" + relatorio.getCorpo() + "&lt;/body&gt;");
		relatorio.getSb()
			.append("&lt;rodape&gt;" + relatorio.getRodape() + "&lt;/footer&gt;");
	}
...
}
	
		</pre>
	</div>


	<br>
	<p>Passo 3 - Extrair a logica do corpo do m�todo aceitar e gerar um
		novo m�todo com o nome visitorSuaClasse, no caso SuaClasse � o nome da
		classe onde foi extraida a logica de acumula��o.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
		
public class TipoArquivoHTML implements TipoArquivo {
...
	public void aceitar(RelatorioEnfermaria relatorio){
		visitarRelatorioEnfermaria(relatorio);
	}
	
	public void visitarRelatorioEnfermaria(RelatorioEnfermaria relatorio) {
		relatorio.getSb()
			.append("&lt;head&gt;" + relatorio.getCabecalho() + "&lt;/head&gt;");
		relatorio.getSb()
			.append("&lt;body&gt;" + relatorio.getCorpo() + "&lt;/body&gt;");
		relatorio.getSb()
			.append("&lt;rodape&gt;" + relatorio.getRodape() + "&lt;/footer&gt;");
	}
...
}
	
		</pre>
	</div>


	<br>

	<p>Passo 4 - Ap�s gerar o novo m�todo aplique o mover m�todo, no
		m�todo "aceitar" para a classe fonte da acumula��o.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
		
public class RelatorioEnfermaria implements Relatorio {
	
	...
	
	public void aceitar(TipoArquivoHTML tipoArquivoHTML){
		tipoArquivoHTML.visitarRelatorioEnfermaria(this);
	}
	
	...
}
	
		</pre>
	</div>

	<br>
	<p>Passo 5 - Aplique a refatora��o, subir m�todo na hierarquia para
		que o m�todo "visitarRelatorioEnfermaria" possa ser implementado pelas
		demais classes que implementam a interface TipoArquivo.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
		
public interface TipoArquivo {

	void visitarRelatorioEnfermaria(RelatorioEnfermaria relatorio);
	
	String imprimir(RelatorioEnfermaria relatorio);
	
}


		</pre>
	</div>

	<br>
	<p>Passo 6 - O mesmo deve ser feito para o m�todo aceitar da classe
		RelatorioEnfermaria, pois assim as demais classes tamb�m podem
		implementar o m�todo.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
	

public interface Relatorio {
	
	void cabecalho(String cabecalho);
	
	void corpo(String corpo);
	
	void rodape(String rodape);
	
	void aceitar(TipoArquivo tipoArquivo);
	
}

		</pre>
	</div>

	<br>
	<p>Passo 7 - Os passos dois, tr�s e quatro devem ser seguidos para
		refatorar demais l�gicas de acumula��o existente ou para a
		implementa��o de novas acumula��es, caso seja necess�rio. Neste caso 
		foi implementada o m�todo visitarRelatorioEnfermaria na classe TipoArquivoXML</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">

public class TipoArquivoXML implements TipoArquivo {
...
	@Override
	public void visitarRelatorioEnfermaria(RelatorioEnfermaria relatorio) {
		relatorio.getSb()
			.append("&lt;cabecalho&gt;" + relatorio.getCabecalho() + "&lt;/cabecalho&gt;");
		relatorio.getSb()
			.append("&lt;corpo&gt;" + relatorio.getCorpo() + "&lt;/corpo&gt;");
		relatorio.getSb()
			.append("&lt;rodape&gt;" + relatorio.getRodape() + "&lt;/rodape&gt;");	
	}
...
}

public class RelatorioMedico implements Relatorio{
...
	@Override
	public void aceitar(TipoArquivo tipoArquivo) {
		tipoArquivo.visitarRelatorioMedico(this);	
	}
...
}
		</pre>
	</div>

	<br>
	<p>passo 8 - compile e teste. Note que o Visitor se encarrega de
		visitar a classe necess�ria para montar o relat�rio. Tornando o c�digo
		mais simples.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
	
public class TestarRelatorio {

	@Test
	public void testarRelatorioHTML(){
		TipoArquivo arquivo = new TipoArquivoHTML();
		Relatorio relatorio = new RelatorioEnfermaria();
		relatorio.cabecalho("Teste Cabe�alho");
		relatorio.corpo("Teste corpo");
		relatorio.rodape("Teste Rodape");
		arquivo.visitarRelatorioEnfermaria((RelatorioEnfermaria) relatorio);
		
		String resultado = arquivo.imprimir((RelatorioEnfermaria) relatorio);
		
		Assert.assertTrue(resultado.contains("html"));
	}
	@Test
	public void testarRelatorioXML(){
		TipoArquivo arquivo = new TipoArquivoXML();
		Relatorio relatorio = new RelatorioEnfermaria();
		relatorio.cabecalho("Teste Cabe�alho");
		relatorio.corpo("Teste corpo");
		relatorio.rodape("Teste Rodape");
		arquivo.visitarRelatorioEnfermaria((RelatorioEnfermaria) relatorio);
		
		String resultado = arquivo.imprimir((RelatorioEnfermaria) relatorio);
		
		Assert.assertTrue(resultado.contains("xml"));
	}
}
		</pre>
	</div>



	<!-- Button trigger modal -->
	<button type="button" class="btn btn-primary btn-lg"
		data-toggle="modal" data-target="#myModal"
		style="position: fixed; bottom: 50px; right: 50px;">Exibir
		Problema</button>

	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<mc:metodosqueacumulaminformacao />
			<div class="modal-content">
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="prettify/prettify_run.js"></script>