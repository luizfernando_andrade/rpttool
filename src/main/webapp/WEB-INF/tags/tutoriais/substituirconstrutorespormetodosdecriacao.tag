<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@taglib tagdir="/WEB-INF/tags/mauscheiros" prefix="mc"%>
<div style="margin-left: 25px;">
	<h2>Tutorial Refatora��o</h2>
	
	<h3>Substituir Construtores por M�todos de Cria��o</h3>
	
	<p>Caso queria acompanhar esse tutorial usando o mesmo contexo basta clicar no link, fazer o download do 
		projeto e buscar a refatora��o deste tutorial. </p>
	
	<a href="https://luizfernando_andrade@bitbucket.org/luizfernando_andrade/torefactor.git">
	projeto_para_download</a>
	<br><br><br>

	<p>Passo 1 - Identificar o contrutor que contenha todos os
		atributos necess�rios para a cria��o do objeto de forma mais completa.
		Este ser� o contrutor principal e ser� utilizado pelos m�todos de
		cria��o.</p>
	<br>
	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
	
package refatoracao.aluno.pojo;

public class Aluno {
...
	public Aluno(String nome, Date nascimento, Integer matricula,Turma turma){
		this.nome = nome;
		this.nascimento = nascimento;
		this.matricula = matricula;
		this.turma = turma;
	}	
...	
}

		</pre>
	</div>
	<br>
	<p>Passo 2 - Selecionado um dos contrutores da classe, e criado um
		m�todo publico e est�tico com nome que expressa o real sentido deste
		construtor retornando um objeto da classe, a partir de lista de
		par�metros.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">


package refatoracao.aluno.pojo;


public class Aluno {
	
	public static Aluno criarAlunoComDependencia(){
		
	}

}

	
		</pre>
	</div>
	
	
	<br>
	<p>Passo 3 - Ap�s criar o m�todo, foi adicionado em sua assinatura,
		a lista de par�metros necess�ria para a cria��o do objeto referente �
		este construtor.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">

package refatoracao.aluno.pojo;


public class Aluno {
	
...
	
	public static Aluno criarAlunoComDependencia(String nome, Date nascimento, 
			Integer matricula, Dependencia dependencia){
	
	}
	
...
}

	
		</pre>
	</div>
	
	
	<br>

	<p>passo 4 - Usando o construtor mais completo, selecionado no
		passo 1 para receber a lista de par�metro. Nos atributos que n�o s�o
		necess�rios ao objeto no momento desta cria��o, passe como par�mentro
		"null".</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
		
package refatoracao.aluno.pojo;


public class Aluno {	

...

	public static Aluno criarAlunoComDependencia(String nome, Date nascimento, 
			Integer matricula, Dependencia dependencia){
		return new Aluno(nome, nascimento, matricula, null, dependencia);
	}

...		
	}
	
		</pre>
	</div>
	
	<br>
	<p>Passo 5 - Localizar onde � usado o construtor modificado para
		que possa ser substituido.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
		
package refatoracao.aluno.teste;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.junit.Test;

import junit.framework.Assert;
import refatoracao.aluno.pojo.Aluno;
import refatoracao.aluno.pojo.Dependencia;

public class TestarAluno {

	@Test
	public void testarAlunoComDependencia(){
		Dependencia dependencia = new Dependencia();
		dependencia.setNomeDisciplina("Matem�tica");
		Calendar nascimento = new GregorianCalendar();
		nascimento.set(1987, 9, 15);
		
		
		Aluno aluno = new Aluno("Marcelo Matos", nascimento.getTime(), 1205110012, dependencia);
		
		Assert.assertTrue(aluno.getDependencia() != null);
	}
}
		</pre>
	</div>
	
	<br>
	<p>Passo 6 Substituir o contrutor pelo m�todo de cria��o com a
		lista par�metros condizentes com a inten��o do m�todo.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
	
package refatoracao.aluno.teste;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.junit.Test;

import junit.framework.Assert;
import refatoracao.aluno.pojo.Aluno;
import refatoracao.aluno.pojo.Dependencia;

public class TestarAluno {

	@Test
	public void testarAlunoComDependencia(){
		Dependencia dependencia = new Dependencia();
		dependencia.setNomeDisciplina("Matem�tica");
		Calendar nascimento = new GregorianCalendar();
		nascimento.set(1987, 9, 15);
		
		
		Aluno aluno = Aluno.criarAlunoComDependencia("Marcelo Matos", 
				nascimento.getTime(), 1205110012, dependencia);
		
		Assert.assertTrue(aluno.getDependencia() != null);
	}
}

		</pre>
	</div>
	
	<br>
	<p>passo 7 - Selecionar um novo construtor e repetir do passo 2 ao
		passo 6 at� que todos os construtores sejam refatorados.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
	
package refatoracao.aluno.pojo;


public class Aluno {	

...

	public static Aluno criarAlunoComDependencia(String nome, Date nascimento, 
			Integer matricula, Dependencia dependencia){
		return new Aluno(nome, nascimento, matricula, null, dependencia);
	}
	public static Aluno criarAlunoComDependenciaETurma(String nome, Date nascimento, 
			Integer matricula,Turma turma, Dependencia dependencia){
		return new Aluno(nome, nascimento, matricula, turma, dependencia);
	}
	public static Aluno criarAlunoComTurma(String nome, Date nascimento, 
			Integer matricula,Turma turma){
		return new Aluno(nome, nascimento, matricula, turma, null);
	}
	public static Aluno criarAluno(String nome, Date nascimento, 
			Integer matricula){
		return new Aluno(nome, nascimento, matricula, null, null);
	}

...		
	}
		</pre>
	</div>
	
	<br>
	<p>passo 8 - Tornar o construtor principal, selecionado no passo 1,
		privado para que n�o possa ser utilizado fora do escopo da classe,
		protegendo a integridade do objeto.</p>

	<div style="width: 720px;">
		<?prettify?>
		<pre class="prettyprint">
	
package refatoracao.aluno.pojo;


public class Aluno {	

...

	private Aluno(String nome, Date nascimento, Integer matricula,
			Turma turma, Dependencia dependencia){
		this.nome = nome;
		this.nascimento = nascimento;
		this.matricula = matricula;
		this.turma = turma;
		this.dependencia = dependencia;
	}
...		
	}
		</pre>
	</div>
	
	
	
	<!-- Button trigger modal -->
<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal"
style="position:fixed;bottom: 50px;right: 50px;">
  Exibir Problema
</button>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
      <mc:excessodeconstrutores/>
    <div class="modal-content">
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>
</div>
<script src="prettify/prettify_run.js"></script>