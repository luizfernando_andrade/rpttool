<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib tagdir="/WEB-INF/tags/mauscheiros" prefix="mc"%>
<%@taglib tagdir="/WEB-INF/tags/tutoriais" prefix="tut"%>
<%@taglib tagdir="/WEB-INF/tags" prefix="tst"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>RPTTool - Refatora��o para Padr�es</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Bootstrap -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
</head>
<body>

	<tst:menu />
	<br>
<ul class="breadcrumb">
    <li><a href="index.jsp">Tipo Problema</a></li>
    <li><a href="filtrarProblemaController?tipoProblema=${tipoProblemaRefatoracao.id}">
    						${tipoProblemaRefatoracao.descricao}</a></li>
   		<li><a href="validarSelecao?mauCheiro=${mauCheiro.id}">${mauCheiro.descricao}</a></li>
   		<li><a href="direcionarRefatoracao?refatoracao=${mauCheiro.id}">${refatoracaoPadrao.descricao}</a></li>
</ul>

	<c:if test="${refatoracaoPadrao.nomeTela eq 'substituirCondicionalPorStrategy'}">
		<div class="col-xs-4">
			<tut:substituircondicionalporstrategy/>
		</div>
	</c:if>
	<c:if test="${refatoracaoPadrao.nomeTela eq 'substituirConstrutoresPorMetodosDeCriacao'}">
		<div class="col-xs-4">
			<tut:substituirconstrutorespormetodosdecriacao/>
		</div>
	</c:if>
	<c:if test="${refatoracaoPadrao.nomeTela eq 'formarTemplateMethod'}">
		<div class="col-xs-4">
			<tut:formarTemplateMethod/>
		</div>
	</c:if>
	<c:if test="${refatoracaoPadrao.nomeTela eq 'moverAcumulacaoParaVisitor'}">
		<div class="col-xs-4">
			<tut:moverAcumulacaoParaVisitor/>
		</div>
	</c:if>
	<c:if test="${refatoracaoPadrao.nomeTela eq 'limitarInstanciacaoComSingleton'}">
		<div class="col-xs-4">
			<tut:limitarinstaciacaocomsingleton/>
		</div>
	</c:if>
	<c:if test="${refatoracaoPadrao.nomeTela eq 'encadearConstrutores'}">
		<div class="col-xs-4">
			<tut:encadearConstrutores/>
		</div>
	</c:if>
	<c:if test="${refatoracaoPadrao.nomeTela eq 'moverConhecimentoCriacaoParaFactory'}">
		<div class="col-xs-4">
			<tut:moverConhecimentoDeCriacaoParaFactory/>
		</div>
	</c:if>
	<c:if test="${refatoracaoPadrao.nomeTela eq 'introduzirCriacaoPolimorficaComFactoryMethod'}">
		<div class="col-xs-4">
			<tut:introduzirCriacaoPolimorficaComFactoryMethod/>
		</div>
	</c:if>
	<c:if test="${refatoracaoPadrao.nomeTela eq 'encapsularClassesComFactory'}">
		<div class="col-xs-4">
			<tut:encapsularClassesComFactory/>
		</div>
	</c:if>
	<c:if test="${refatoracaoPadrao.nomeTela eq 'encapsularCompositeComBuilder'}">
		<div class="col-xs-4">
			<tut:encapsularCompositeComBuilder/>
		</div>
	</c:if>
	<c:if test="${refatoracaoPadrao.nomeTela eq 'internalizarSingleton'}">
		<div class="col-xs-4">
			<tut:internalizarSingleton/>
		</div>
	</c:if>
	<c:if test="${refatoracaoPadrao.nomeTela eq 'comporMetodo'}">
		<div class="col-xs-4">
			<tut:comporMetodo/>
		</div>
	</c:if>
	<c:if test="${refatoracaoPadrao.nomeTela eq 'moverEmbelezamentoParaDecorator'}">
		<div class="col-xs-4">
			<tut:moverEmbelezamentoParaDecorator/>
		</div>
	</c:if>
	<c:if test="${refatoracaoPadrao.nomeTela eq 'subtituirCondicionaisPorState'}">
		<div class="col-xs-4">
			<tut:substituirCondicionaisPorState/>
		</div>
	</c:if>
	<c:if test="${refatoracaoPadrao.nomeTela eq 'substituirEnvioCondicionalPorCommand'}">
		<div class="col-xs-4">
			<tut:substituirEnvioCondicionalPorCommand/>
		</div>
	</c:if>
	<c:if test="${refatoracaoPadrao.nomeTela eq 'substituirArvorePorComposite'}">
		<div class="col-xs-4">
			<tut:substituirArvorePorComposite/>
		</div>
	</c:if>
	<c:if test="${refatoracaoPadrao.nomeTela eq 'extrairComposite'}">
		<div class="col-xs-4">
			<tut:extrairComposite/>
		</div>
	</c:if>
	<c:if test="${refatoracaoPadrao.nomeTela eq 'substituirUmMuitosPorComposite'}">
		<div class="col-xs-4">
			<tut:substituirUmMuitosPorComposite/>
		</div>
	</c:if>
	<c:if test="${refatoracaoPadrao.nomeTela eq 'substituirNotificacaoPorObserver'}">
		<div class="col-xs-4">
			<tut:substituirNotificacaoPorObserver/>
		</div>
	</c:if>
	<c:if test="${refatoracaoPadrao.nomeTela eq 'unificarInterfacesComAdapter'}">
		<div class="col-xs-4">
			<tut:unificarInterfacesComAdapter/>
		</div>
	</c:if>
	<c:if test="${refatoracaoPadrao.nomeTela eq 'extrairAdapter'}">
		<div class="col-xs-4">
			<tut:extrairAdapter/>
		</div>
	</c:if>
	<c:if test="${refatoracaoPadrao.nomeTela eq 'substituirLinguagemImplicitaPorInterpreter'}">
		<div class="col-xs-4">
			<tut:substituirLinguagemImplicitaPorInterpreter/>
		</div>
	</c:if>
	<c:if test="${refatoracaoPadrao.nomeTela eq 'moverAcumulacaoParaParametroColetor'}">
		<div class="col-xs-4">
			<tut:moverAcumulacaoParaParametroColetor/>
		</div>
	</c:if>
	<c:if test="${refatoracaoPadrao.nomeTela eq 'substituirCodigoDeTipoPorClasse'}">
		<div class="col-xs-4">
			<tut:substituirCodigoDeTipoPorClasse/>
		</div>
	</c:if>
	<c:if test="${refatoracaoPadrao.nomeTela eq 'introduzirObjetoNulo'}">
		<div class="col-xs-4">
			<tut:introduzirObjetoNulo/>
		</div>
	</c:if>
	<c:if test="${refatoracaoPadrao.nomeTela eq 'unificarInterfaces'}">
		<div class="col-xs-4">
			<tut:unificarInterfaces/>
		</div>
	</c:if>
	<c:if test="${refatoracaoPadrao.nomeTela eq 'extrairParametro'}">
		<div class="col-xs-4">
			<tut:extrairParametro/>
		</div>
	</c:if>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.js"></script>
</body>
</html>