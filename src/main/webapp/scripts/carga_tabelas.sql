create database rpttool;

use rpttool;

INSERT INTO tipoproblemarefatoracao (id, descricao, nomeTela) VALUES (1, 'Criação', 'criacao');
INSERT INTO tipoproblemarefatoracao (id, descricao, nomeTela) VALUES (2, 'Simplificação', 'simplificacao');
INSERT INTO tipoproblemarefatoracao (id, descricao, nomeTela) VALUES (3, 'Generalização', 'generalizacao');
INSERT INTO tipoproblemarefatoracao (id, descricao, nomeTela) VALUES (4, 'Proteção', 'protecao');
INSERT INTO tipoproblemarefatoracao (id, descricao, nomeTela) VALUES (5, 'Acumulação', 'acumulacao');
INSERT INTO tipoproblemarefatoracao (id, descricao, nomeTela) VALUES (6, 'Utilitários', 'utilitarios');

------------------------------------------------------------------------------------------------
/* CRIAÇÃO */
INSERT INTO refatoracaopadrao (
	id, 
	tipoProblema_id,
	descricao,
	nomeTela
    ) 
VALUES (
	1,
	1,
	'Substituir Construtores por métodos de criação',
	'substituirConstrutoresPorMetodosDeCriacao');
    
    INSERT INTO refatoracaopadrao (
	id, 
	tipoProblema_id,
	descricao,
	nomeTela
    ) 
VALUES (
	7,
	1,
	'Mover Conhecimento de Criação para Factory',
	'moverConhecimentoCriacaoParaFactory');
    
     
    INSERT INTO refatoracaopadrao (
	id, 
	tipoProblema_id,
	descricao,
	nomeTela
    ) 
VALUES (
	8,
	1,
	'Encapsular Classes com Factory',
	'encapsularClassesComFactory');
    
	INSERT INTO refatoracaopadrao (
	id, 
	tipoProblema_id,
	descricao,
	nomeTela
    ) 
VALUES (
	9,
	1,
	'Introduzir Criação polimórfica com Factory Method',
	'introduzirCriacaoPolimorficaComFactoryMethod');
    
	INSERT INTO refatoracaopadrao (
	id, 
	tipoProblema_id,
	descricao,
	nomeTela
    ) 
VALUES (
	10,
	1,
	'Encapsular Composite com Builder',
	'encapsularCompositeComBuilder');
    
	INSERT INTO refatoracaopadrao (
	id, 
	tipoProblema_id,
	descricao,
	nomeTela
    ) 
VALUES (
	11,
	1,
	'Internalizar Singleton',
	'internalizarSingleton');
    
insert into maucheiro (id, descricao, refatoracaoPadrao_id, nomeImagem) 
values (1,'Excesso de Construtores na Classe',1,'excessoDeConstrutores');

insert into maucheiro (id, descricao, refatoracaoPadrao_id, nomeImagem) 
values (2,'Espalhamento de Criação',7,'espalhamentoCriacao');


insert into maucheiro (id, descricao, refatoracaoPadrao_id, nomeImagem) 
values (3,'Reduzir Visibilidade de Instâncias',8,'reduzirVisibilidadeDeInstancias');

insert into maucheiro (id, descricao, refatoracaoPadrao_id, nomeImagem) 
values (4,'Construtores comuns  em classes filhas',9,'construtoresComunsEmClassesFilhas');

insert into maucheiro (id, descricao, refatoracaoPadrao_id, nomeImagem) 
values (5,'Criação Complexa de Objetos',10,'criacaoComplexaDeObjetos');

insert into maucheiro (id, descricao, refatoracaoPadrao_id, nomeImagem) 
values (6,'Singletons desnecessários',11,'singletonDesnecessario');

------------------------------------------------------------------------------------------------
/* SIMPLIFICAÇÃO */
INSERT INTO refatoracaopadrao (
	id, 
	tipoProblema_id,
	descricao,
	nomeTela
    ) 
VALUES (
	2,
	2,
	'Substituir Lógica Condicional por Strategy',
	'substituirCondicionalPorStrategy');
    
    INSERT INTO refatoracaopadrao (
	id, 
	tipoProblema_id,
	descricao,
	nomeTela
    ) 
VALUES (
	12,
	2,
	'Compor Método',
	'comporMetodo');
    
	INSERT INTO refatoracaopadrao (
	id, 
	tipoProblema_id,
	descricao,
	nomeTela
    ) 
VALUES (
	13,
	2,
	'Mover Embelezamento para Decorator',
	'moverEmbelezamentoParaDecorator');
	
    INSERT INTO refatoracaopadrao (
	id, 
	tipoProblema_id,
	descricao,
	nomeTela
    ) 
VALUES (
	14,
	2,
	'Substituir Condicionais que alteram estado para State',
	'subtituirCondicionaisPorState');

	INSERT INTO refatoracaopadrao (
	id, 
	tipoProblema_id,
	descricao,
	nomeTela
    ) 
VALUES (
	15,
	2,
	'Substituir árvore implicita por Composite',
	'substituirArvorePorComposite');
    
	INSERT INTO refatoracaopadrao (
	id, 
	tipoProblema_id,
	descricao,
	nomeTela
    ) 
VALUES (
	16,
	2,
	'Substituir envio condicional por Command',
	'substituirEnvioCondicionalPorCommand');
    
insert into maucheiro (id, descricao, refatoracaoPadrao_id, nomeImagem) 
values (7,'Excesso de lógica condicional',2,'excessoLogicaCondicional');

insert into maucheiro (id, descricao, refatoracaoPadrao_id, nomeImagem) 
values (8,'Excesso de responsabilidade em classes',13,'excessoResponsabilidadeClasse');


insert into maucheiro (id, descricao, refatoracaoPadrao_id, nomeImagem) 
values (9,'Condicionais que mudam estado',14,'condicionaisMudaEstado');


insert into maucheiro (id, descricao, refatoracaoPadrao_id, nomeImagem) 
values (10,'Rastreabilidade em estrutura complexas',15,'rastreabilidadeEstruturaComplexa');


insert into maucheiro (id, descricao, refatoracaoPadrao_id, nomeImagem) 
values (11,'Condicionais que projetam ação',16,'condicionaisAcao');
update maucheiro set refatoracaoPadrao_id = 16 where id = 11;

insert into maucheiro (id, descricao, refatoracaoPadrao_id, nomeImagem) 
values (12,'Métodos com muitas responsabilidades',12,'metodoComMuitasResponsabilidades');

------------------------------------------------------------------------------------------------
/* GENERALIZAÇÃO */

INSERT INTO refatoracaopadrao (
	id, 
	tipoProblema_id,
	descricao,
	nomeTela
    ) 
VALUES (
	3,
	3,
	'Formar Template Method',
	'formarTemplateMethod');
    
    INSERT INTO refatoracaopadrao (
	id, 
	tipoProblema_id,
	descricao,
	nomeTela
    ) 
VALUES (
	17,
	3,
	'Extrair Composite',
	'extrairComposite');
    
	INSERT INTO refatoracaopadrao (
	id, 
	tipoProblema_id,
	descricao,
	nomeTela
    ) 
VALUES (
	18,
	3,
	'Substituir Distinção um/muitos por Composite',
	'substituirUmMuitosPorComposite');
    
	INSERT INTO refatoracaopadrao (
	id, 
	tipoProblema_id,
	descricao,
	nomeTela
    ) 
VALUES (
	19,
	3,
	'Substituir Notificações Hard-Code por Observer',
	'substituirNotificacaoPorObserver');
    
	INSERT INTO refatoracaopadrao (
	id, 
	tipoProblema_id,
	descricao,
	nomeTela
    ) 
VALUES (
	20,
	3,
	'Unificar Interfaces com Adapter',
	'unificarInterfacesComAdapter');
    
    INSERT INTO refatoracaopadrao (
	id, 
	tipoProblema_id,
	descricao,
	nomeTela
    ) 
VALUES (
	21,
	3,
	'Extrair Adapter',
	'extrairAdapter');
    
	INSERT INTO refatoracaopadrao (
	id, 
	tipoProblema_id,
	descricao,
	nomeTela
    ) 
VALUES (
	22,
	3,
	'Substituir Linguagem implícita por Interpreter',
	'substituirLinguagemImplicitaPorInterpreter');
    
insert into maucheiro (id, descricao, refatoracaoPadrao_id, nomeImagem) 
values (13,'Implementação de Métodos Semelhantes em Hierarquia',3,'metodosSemelhantesEmHierarquia');

insert into maucheiro (id, descricao, refatoracaoPadrao_id, nomeImagem) 
values (14,'Compisites espalhadnos na Hierarquia',17,'compositeEspalhadoEmHierarquia');


insert into maucheiro (id, descricao, refatoracaoPadrao_id, nomeImagem) 
values (15,'Manipulação Duplicada de Objetos um/muitos',18,'manipulacaoDuplicadaObjetos');

insert into maucheiro (id, descricao, refatoracaoPadrao_id, nomeImagem) 
values (16,'Classe Observadora em Hierarquia',19,'classeObservadoraEmHierarquia');

insert into maucheiro (id, descricao, refatoracaoPadrao_id, nomeImagem) 
values (17,'Classes com uma interface preferida',20,'classesComInterfacePreferida');

insert into maucheiro (id, descricao, refatoracaoPadrao_id, nomeImagem) 
values (18,'Código Direcionado a Versões',21,'codigoDirecionadoVersoes');

insert into maucheiro (id, descricao, refatoracaoPadrao_id, nomeImagem) 
values (19,'Métodos Com Elementos de Linguagem Implícita',22,'metodosComElementosLiguagemImplicita');

------------------------------------------------------------------------------------------------
/* Acumulação */

INSERT INTO refatoracaopadrao (
	id, 
	tipoProblema_id,
	descricao,
	nomeTela
    ) 
VALUES (
	4,
	5,
	'Mover acumulação para Visitor',
	'moverAcumulacaoParaVisitor');
    
    INSERT INTO refatoracaopadrao (
	id, 
	tipoProblema_id,
	descricao,
	nomeTela
    ) 
VALUES (
	23,
	5,
	'Mover acumulação para Parâmetro Coletor',
	'moverAcumulacaoParaParametroColetor');
    
insert into maucheiro (id, descricao, refatoracaoPadrao_id, nomeImagem) 
values (20,'Métodos que acumulam informação',4,'metodosqueacumulaminformacao');

insert into maucheiro (id, descricao, refatoracaoPadrao_id, nomeImagem) 
values (21,'Objetos que colhem informação.',23,'objetosquecolheminformacao');

------------------------------------------------------------------------------------------------

/* Protecao */

INSERT INTO refatoracaopadrao (
	id, 
	tipoProblema_id,
	descricao,
	nomeTela
    ) 
VALUES (
	5,
	4,
	'Limitar Instanciação com Singleton',
	'limitarInstanciacaoComSingleton');
    
    INSERT INTO refatoracaopadrao (
	id, 
	tipoProblema_id,
	descricao,
	nomeTela
    ) 
VALUES (
	24,
	4,
	'Substituir código de tipo por classe',
	'substituirCodigoDeTipoPorClasse');
    
    INSERT INTO refatoracaopadrao (
	id, 
	tipoProblema_id,
	descricao,
	nomeTela
    ) 
VALUES (
	25,
	4,
	'Introduzir Objeto Nulo',
	'introduzirObjetoNulo');
    
insert into maucheiro (id, descricao, refatoracaoPadrao_id, nomeImagem) 
values (22,'Multiplas Instâncias ',5,'multiplasInstancias');

insert into maucheiro (id, descricao, refatoracaoPadrao_id, nomeImagem) 
values (23,'Excesso de Atributos com Valor Fixo',24,'excessoDeAtributoComValorFixo');


insert into maucheiro (id, descricao, refatoracaoPadrao_id, nomeImagem) 
values (24,'Tratamento de objeto nulo',25,'tratamentoDeObjetoNulo');


------------------------------------------------------------------------------------------------

/* Utilitários */

INSERT INTO refatoracaopadrao (
	id, 
	tipoProblema_id,
	descricao,
	nomeTela
    ) 
VALUES (
	6,
	6,
	'Encadear Construtores',
	'encadearConstrutores');
    
    INSERT INTO refatoracaopadrao (
	id, 
	tipoProblema_id,
	descricao,
	nomeTela
    ) 
VALUES (
	26,
	6,
	'Unificar Interfaces',
	'unificarInterfaces');
    
    INSERT INTO refatoracaopadrao (
	id, 
	tipoProblema_id,
	descricao,
	nomeTela
    ) 
VALUES (
	27,
	6,
	'Extrair Parâmetro',
	'extrairParametro');
    
insert into maucheiro (id, descricao, refatoracaoPadrao_id, nomeImagem) 
values (25,'Construtores com código duplicado',6,'construtoresComCodigoDuplicado');

insert into maucheiro (id, descricao, refatoracaoPadrao_id, nomeImagem) 
values (26,'Necessidade de manipulação poliformica',26,'manipulacaoPoliformica');


insert into maucheiro (id, descricao, refatoracaoPadrao_id, nomeImagem) 
values (27,'Alterar valor em objeto recem criado',27,'alterarValorEmObjetoRecemCriado');



    