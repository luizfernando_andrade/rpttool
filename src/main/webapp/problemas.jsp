<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib tagdir="/WEB-INF/tags/mauscheiros" prefix="mc"%>
<%@taglib tagdir="/WEB-INF/tags" prefix="tst"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>RPTTool - Maus Cheiros</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Bootstrap -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
</head>
<body>

<tst:menu/>
<br>

<ul class="breadcrumb">
    <li><a href="index.jsp">Tipo Problema</a></li>
    <li><a href="filtrarProblemaController?tipoProblema=${tipoProblemaRefatoracao.id}">
    						${tipoProblemaRefatoracao.descricao}</a></li>
   	<c:if test="${mauCheiro ne null}">
   		<li><a href="validarSelecao?mauCheiro=${mauCheiro.id}">${mauCheiro.descricao}</a></li>
   	</c:if>
</ul>

<br>
<div class="row" style="margin-left: 25px;">
	<div class="col-xs-4">
		<form action="validarSelecao" method="post">
			
			<c:forEach items ="${mausCheiros}" var="problema">
			  <div class="radio">
				  <label><input type="radio" name="mauCheiro" required 
				  value="${problema.id}"> ${problema.descricao} </label>  
			 </div>
			</c:forEach>
			
			<button type="submit" class="btn btn-default">Exibir Exemplo</button>
		</form>
	</div>
	<c:if test="${mauCheiro ne null}">
		<div class="col-xs-4">
		<h2>Exemplo mau cheiro:</h2> 
		<br>
			<c:if test="${mauCheiro.nomeImagem eq 'excessoLogicaCondicional'}">
				<mc:excessodelogicacondicional/>		
			</c:if>
			<c:if test="${mauCheiro.nomeImagem eq 'excessoDeConstrutores'}">
				<mc:excessodeconstrutores/>		
			</c:if>
			<c:if test="${mauCheiro.nomeImagem eq 'metodosSemelhantesEmHierarquia'}">
				<mc:metodossemelhantesemhierarquia/>		
			</c:if>
			<c:if test="${mauCheiro.nomeImagem eq 'metodosqueacumulaminformacao'}">
				<mc:metodosqueacumulaminformacao/>		
			</c:if>
			<c:if test="${mauCheiro.nomeImagem eq 'multiplasInstancias'}">
				<mc:multiplasinstancias/>		
			</c:if>
			<c:if test="${mauCheiro.nomeImagem eq 'construtoresComCodigoDuplicado'}">
				<mc:construtorescomcodigoduplicado/>		
			</c:if>
			<c:if test="${mauCheiro.nomeImagem eq 'espalhamentoCriacao'}">
				<mc:espalhamentoCriacao/>	
			</c:if>
			<c:if test="${mauCheiro.nomeImagem eq 'construtoresComunsEmClassesFilhas'}">
				<mc:construtoresComunsEmClassesFilhas/>
			</c:if>
			<c:if test="${mauCheiro.nomeImagem eq 'reduzirVisibilidadeDeInstancias'}">
				<mc:reduzirVisibilidadeDeInstancias/>
			</c:if>
			<c:if test="${mauCheiro.nomeImagem eq 'criacaoComplexaDeObjetos'}">
				<mc:criacaoComplexadeObjetos/>
			</c:if>
			<c:if test="${mauCheiro.nomeImagem eq 'singletonDesnecessario'}">
				<mc:singletonDesnecessario/>
			</c:if>
			<c:if test="${mauCheiro.nomeImagem eq 'metodoComMuitasResponsabilidades'}">
				<mc:metodoComMuitasResponsabilidades/>
			</c:if>
			<c:if test="${mauCheiro.nomeImagem eq 'excessoResponsabilidadeClasse'}">
				<mc:excessoResponsabilidadeClasse/>
			</c:if>
			<c:if test="${mauCheiro.nomeImagem eq 'condicionaisMudaEstado'}">
				<mc:condicionaisMudamEstado/>
			</c:if>
			<c:if test="${mauCheiro.nomeImagem eq 'condicionaisAcao'}">
				<mc:condicionaisProjetamAcao/>
			</c:if>
			<c:if test="${mauCheiro.nomeImagem eq 'rastreabilidadeEstruturaComplexa'}">
				<mc:rastreabilidadeEstruturaComplexa/>
			</c:if>
			<c:if test="${mauCheiro.nomeImagem eq 'compositeEspalhadoEmHierarquia'}">
				<mc:compositeEspalhadoEmHierarquia/>
			</c:if>
			<c:if test="${mauCheiro.nomeImagem eq 'manipulacaoDuplicadaObjetos'}">
				<mc:manipulacaoDuplicadaObjetos/>
			</c:if>
			<c:if test="${mauCheiro.nomeImagem eq 'classeObservadoraEmHierarquia'}">
				<mc:classeObservadoraHierarquia/>
			</c:if>
			<c:if test="${mauCheiro.nomeImagem eq 'classesComInterfacePreferida'}">
				<mc:classesComInterfacePreferida/>
			</c:if>
			<c:if test="${mauCheiro.nomeImagem eq 'codigoDirecionadoVersoes'}">
				<mc:codigoDirecionadoVersoes/>
			</c:if>
			<c:if test="${mauCheiro.nomeImagem eq 'metodosComElementosLiguagemImplicita'}">
				<mc:metodosComElementosLiguagemImplicita/>
			</c:if>
			<c:if test="${mauCheiro.nomeImagem eq 'objetosquecolheminformacao'}">
				<mc:objetosquecolheminformacao/>
			</c:if>
			<c:if test="${mauCheiro.nomeImagem eq 'excessoDeAtributoComValorFixo'}">
				<mc:excessoDeAtributoComValorFixo/>
			</c:if>
			<c:if test="${mauCheiro.nomeImagem eq 'tratamentoDeObjetoNulo'}">
				<mc:tratamentoDeObjetoNulo/>
			</c:if>
			<c:if test="${mauCheiro.nomeImagem eq 'manipulacaoPoliformica'}">
				<mc:manipulacaoPoliformica/>
			</c:if>
			<c:if test="${mauCheiro.nomeImagem eq 'alterarValorEmObjetoRecemCriado'}">
				<mc:alterarValorEmObjetoRecemCriado/>
			</c:if>
		<br>
		<a href="direcionarRefatoracao?refatoracao=${mauCheiro.id}" 
		class="btn btn-default">Exibir Refatoração</a>

		</div>
	</c:if>
</div>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.js"></script>
</body>
</html>